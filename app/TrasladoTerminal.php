<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class TrasladoTerminal extends Model
{

	protected $fillable = [
		'terminal_id',
		'origen_id',
		'destino_id'
	];


	public function origen(){
		return $this->belongsTo(Sucursales::class,'origen_id','id');
	}

	public function destino(){
		return $this->belongsTo(Sucursales::class,'destino_id','id');
	}
}
