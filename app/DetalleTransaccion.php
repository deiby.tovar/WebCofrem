<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class DetalleTransaccion extends Model
{
    public static $DESCRIPCION_ADMINISTRACION = "A";
    public static $DESCRIPCION_PLASTICO = "P";
    public static $DESCRIPCION_CONSUMO = "C";

    protected $table = "detalle_transacciones";


    public function detalleProducto(){
    	return $this->belongsTo(DetalleProdutos::class);
	}

	public function transaccion(){
    	return $this->belongsTo(Transaccion::class,'transaccion_id','id');
	}
}
