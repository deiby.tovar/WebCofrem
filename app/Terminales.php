<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Terminales extends Model implements AuditableContract
{

	protected $fillable = [
		'sucursal_id',
		'codigo',
		'uid',
		'mac',
		'imei',
		'celular',
		'numero_activo',
		'password',
		'estado',
	];




    //
    public static $ESTADO_TERMINAL_ACTIVA = "A";
    public static $ESTADO_TERMINAL_INACTIVA = "I";
    use Auditable;

    public static $TERMINAL_ESTADO_ACTIVA = 'A';
    public static $TERMINAL_ESTADO_INACTIVA = 'I';

    public function getSucursal(){
        return $this->belongsTo('creditocofrem\Sucursales','sucursal_id','id');
    }

    public function traslados(){
    	return $this->hasMany(TrasladoTerminal::class,'terminal_id','id');
	}
}
