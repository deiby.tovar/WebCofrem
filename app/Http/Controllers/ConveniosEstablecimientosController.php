<?php

	namespace creditocofrem\Http\Controllers;

	use Carbon\Carbon;
	use creditocofrem\Auditoria;
	use creditocofrem\ConveniosEsta;
	use creditocofrem\Establecimientos;
	use creditocofrem\FerecuenciaConvEs;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\PlazoConvEs;
	use creditocofrem\RangoConvEs;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Yajra\Datatables\Datatables;


	class ConveniosEstablecimientosController extends Controller {

		/**
		 * carga el data table con la lista de cnvenios existente para un establecimiento
		 *
		 * @param Request $request id del establecimiento
		 *
		 * @return mixed
		 */
		public function gridConveniosEstablecimiento(Request $request) {
			//se consultan los convenios existentes para el establecimiento
			$convenios = ConveniosEsta::where('establecimiento_id', $request->id)->get();

			//se envia la lista de establecimientos al datatable en el formato que espera.
			return Datatables::of($convenios)
							 ->addColumn('action', function ($convenios) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<a href="'.route("coveniosesta.reglas", ["id" => $convenios->id])
									 .'" data-modal="modal-lg" class="btn btn-xs btn-custom" ><i class="ti-pencil-alt"></i> Reglas</a>';
								 $acciones = $acciones.'<a class="btn btn-xs btn-primary" href="'.route("convenios.historial", [$convenios->id])
									 .'" ><i class="ti-layers-alt"></i> Historial</a>';
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * trae la vista del modal que permite agregar un nuevo convenio a un establecimiento
		 *
		 * @param $id id correspondiente al establecimiento al que se le quiere agregar un convenio.
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearConvenio($id) {
			$establecimiento_id = $id;

			return view('establecimientos.convenios.modalcrearconvenio', compact('establecimiento_id'));
		}

		/**
		 * metodo que permite crear un convenio a un determinado estableciminto comercial
		 *
		 * @param Request $request datos correspondientes al convenios
		 * @param         $id      id referente al establecimiento al que se le quiere crear el convenio
		 *
		 * @return array
		 */
		public function crearConvenio(Request $request, $id) {
			DB::beginTransaction();//iniciamos una transacion en la base de datos, para evitar datos huerfanos en caso de error
			try {
				//se consulta si existe un convenio activo para el establecimiento
				$exite = ConveniosEsta::query()
									  ->where('establecimiento_id', $id)
									  ->whereIn('estado', [ConveniosEsta::ACTIVA, ConveniosEsta::PENDIENTE])
									  ->exists();
				if ($exite) { // de existir un convenio activo retornamos un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => 'Ya existe un convenio activo para este establecimiento.',
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_INTERNAL_SERVER);
				}

				$validator = \Validator::make($request->all(), [
					'numero_convenio' => 'required|unique:convenios_estas',
				]);

				if ($validator->fails()) {//si no cumple con la validacion retorna un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}
				//instanciamos un objeto de la clase ConveniosEsta y inicializamos con lo que tre el request
				$convenioEsta                     = new ConveniosEsta($request->all());
				$convenioEsta->establecimiento_id = $id; //lo asociamos al establecimiento
				$establecimiento                  = Establecimientos::query()->find($id);//consultamos el establecimiento por el id

				// si la fecha de inicio del convenio es menor a la fecha actual, creamos el convenio como activo
				if ($convenioEsta->fecha_inicio <= Carbon::now()->format('d/m/Y')) {
					$convenioEsta->estado    = ConveniosEsta::ACTIVA;
					$establecimiento->estado = Establecimientos::ACTIVA;// actualizamos el estado del establecimiento a activo
					$establecimiento->save();//guardamos los cambios del establecimiento
				}
				else {// si la fecha de inicio es mayor definimos el estado de convenio en pendiente
					$convenioEsta->estado = ConveniosEsta::PENDIENTE;
				}
				$convenioEsta->fecha_inicio = Carbon::createFromFormat('d/m/Y', $convenioEsta->fecha_inicio);
				$convenioEsta->fecha_fin    = Carbon::createFromFormat('d/m/Y', $convenioEsta->fecha_fin);
				$convenioEsta->save();//guardamos los cambios
				DB::commit();//guardamos los cambios en la base de datos

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $convenioEsta,
				], CodesResponse::CODE_CREATED);

			} catch (\Exception $exception) {
				DB::rollBack();//en caso de error descartamos los cambios en la base de dato

				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * metodo que trae la vista para modificar o crear los parametros a un convenio
		 *
		 * @param $id id referente al convenio a parametrizar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewReglasConvenioEstablecimiento($id) {
			$plazo      = PlazoConvEs::query()->where('convenios_esta_id', $id)->first();
			$frecuencia = FerecuenciaConvEs::where('convenios_esta_id', $id)->first();
			$convenio   = ConveniosEsta::find($id);

			return view('establecimientos.convenios.modalreglasconvenioestablecimiento',
				['plazo' => $plazo, 'frecuencia' => $frecuencia, 'convenio' => $convenio]);
		}

		/**
		 * metodo que carga los rangos que se han configurado para un convenio
		 *
		 * @param $id id correspondiente al convenio
		 *
		 * @return mixed
		 */
		public function gridRangosConvenio($id) {
			$rangos = RangoConvEs::query()->where('convenios_esta_id', $id)->where('estado', 'A')->get();

			return Datatables::of($rangos)
							 ->addColumn('action', function ($rangos) {
								 $maxId = RangoConvEs::where('convenios_esta_id', $rangos->convenios_esta_id)
													 ->where('dias', $rangos->dias)
													 ->select([\DB::raw('MAX(id) as id')])
													 ->first();
								 if ($rangos->id == $maxId->id) {
									 $acciones = '<div class="btn-group">';
									 $acciones = $acciones.'<button class="btn btn-xs btn-danger" onclick="eliminarRango('.$rangos->id
										 .')" ><i class="fa fa-trash"></i> Eliminar</button>';
									 $acciones = $acciones.'</div>';

									 return $acciones;
								 }
							 })
							 ->make(true);
		}

		/**
		 * funcion que permite actualizar el plazo de pago de un convenio
		 *
		 * @param Request $request datos de los dias de plazon para el pago
		 * @param         $id      id correspondiente al convenio a actualizar
		 *
		 * @return array
		 */
		public function actualizarPlazoPagoConvenio(Request $request, $id) {
			$result = [];
			try {
				$plazo = PlazoConvEs::where('convenios_esta_id', $id)->first();
				if ($plazo == null) {
					$plazo                    = new PlazoConvEs();
					$plazo->dias              = $request->dias;
					$plazo->convenios_esta_id = $id;
				}
				else {
					$plazo->dias = $request->dias;
				}
				$plazo->save();
				$result['estado']  = true;
				$result['mensaje'] = 'El valor del plazo del pago acualizado.';
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible actualizar el plazo de pago. '.$exception->getMessage();
			}

			return $result;
		}

		/**
		 * funcion encargada de actualizar o crear una frecuencia de corte para un convenio
		 *
		 * @param Request $request dato de la frecuencia de corte
		 * @param         $id      id correspondiente al convenio a actulizar su frecuencia de corte
		 *
		 * @return array
		 */
		public function actualizarFrecuenciaCorteConvenio(Request $request, $id) {
			$result = [];
			try {
				$frecuencia = FerecuenciaConvEs::where('convenios_esta_id', $id)->first();
				if ($frecuencia == null) {
					$frecuencia                    = new FerecuenciaConvEs();
					$frecuencia->frecuencia_corte  = $request->frecuencia_corte;
					$frecuencia->convenios_esta_id = $id;
				}
				else {
					$frecuencia->frecuencia_corte = $request->frecuencia_corte;
				}
				$frecuencia->save();
				$result['estado']  = true;
				$result['mensaje'] = 'El valor del plazo del pago acualizado.';
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible actualizar el plazo de pago. '.$exception->getMessage();
			}

			return $result;
		}

		/**
		 * metodo encargado de agregar un nuevo rango de administracion a un convenio
		 *
		 * @param Request $request datos de los rangos a ingresar
		 * @param         $id      id correspondiente al convenio
		 *
		 * @return array
		 */
		public function nuevoRongoConvenio(Request $request, $id) {
			$result = [];
			try {
				$rangos = RangoConvEs::where('convenios_esta_id', $id)->where('estado', 'A')->get();

				if (count($rangos) > 0) {
					foreach ($rangos as $rango1) {
						if ($rango1->dias == $request->dias) {
							if ($rango1->valor_max >= str_replace(",", ".", str_replace(".", "", $request->valor_min))) {
								$result['estado']  = false;
								$result['mensaje'] = 'Para este plazo de la factura, exite un rango de precios que se cruza con el que trata de agregar';

								return $result;
							}
						}
					}
				}
				$rango                    = new RangoConvEs();
				$rango->valor_min         = str_replace(",", ".", str_replace(".", "", $request->valor_min));
				$rango->valor_max         = str_replace(",", ".", str_replace(".", "", $request->valor_max));
				$rango->dias              = $request->dias;
				$rango->porcentaje        = $request->porcentaje;
				$rango->convenios_esta_id = $id;
				$rango->save();
				$result['estado']  = true;
				$result['mensaje'] = 'rango agregado correctamente';
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible agregar el nuevo rango';
			}

			return $result;
		}

		/**
		 * metodo que elimina un rago de administracion para un convenio
		 *
		 * @param Request $request trae la id corrrespodiente al rango a eliminar
		 *
		 * @return array
		 */
		public function eliminarRangoConvenio(Request $request) {
			$result = [];
			try {
				$rango         = RangoConvEs::find($request->id);
				$rango->estado = 'I';
				$rango->save();
				$result['estado']  = true;
				$result['mensaje'] = 'Rango eliminado satisfactoriamente.';
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible elimnar el rango.';
			}

			return $result;
		}

		public function historialConvenioEstablecimiento($id) {
			$convenio = ConveniosEsta::find($id);

			return view('establecimientos.convenios.historicoconvenio', compact('convenio'));
		}

		public function gridHitorialRangos($id) {
			$rangos    = RangoConvEs::where('convenios_esta_id', $id)->distinct()->select('id')->pluck('id');
			$auditoria = Auditoria::where('auditable_type', 'creditocofrem\RangoConvEs')
								  ->join('users', 'users.id', 'audits.user_id')
								  ->whereIn('auditable_id', $rangos->all())
								  ->select(['audits.*', 'users.name'])
								  ->get();

			return Datatables::of($auditoria)->make(true);
		}

		public function gridFrecuencia($id) {
			$rangos    = FerecuenciaConvEs::where('convenios_esta_id', $id)->distinct()->select('id')->pluck('id');
			$auditoria = Auditoria::where('auditable_type', 'creditocofrem\FerecuenciaConvEs')
								  ->join('users', 'users.id', 'audits.user_id')
								  ->whereIn('auditable_id', $rangos->all())
								  ->select(['audits.*', 'users.name'])
								  ->get();

			return Datatables::of($auditoria)->make(true);
		}

		public function gridPlazos($id) {
			$rangos    = PlazoConvEs::where('convenios_esta_id', $id)->distinct()->select('id')->pluck('id');
			$auditoria = Auditoria::where('auditable_type', 'creditocofrem\PlazoConvEs')
								  ->join('users', 'users.id', 'audits.user_id')
								  ->whereIn('auditable_id', $rangos->all())
								  ->select(['audits.*', 'users.name'])
								  ->get();

			return Datatables::of($auditoria)->make(true);
		}


	}
