<?php

	namespace creditocofrem\Http\Controllers;

	use Carbon\Carbon;
	use creditocofrem\DetalleProdutos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Globals\Response;
	use creditocofrem\HEstadoTransaccion;
	use creditocofrem\Personas;
	use creditocofrem\Tarjetas;
	use creditocofrem\Transaccion;
	use Illuminate\Http\Request;

	class ApiReportesController extends Controller {

		/**
		 * metodo que regresa la ultima transaccion hecha para esta terminal
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function ultimaTransaccion(Request $request) {
			try {
				if ($request->has('codigo_terminal')) {
					$transaccion = Transaccion::with('detalleTransaccion', 'htransanccionone')
											  ->withCount([
												  'detalleTransaccion as valor' => function ($query) {
													  $query->select([\DB::raw('SUM(valor)')]);
												  },
											  ])
											  ->where('codigo_terminal', $request->codigo_terminal)
											  ->orderBy('fecha', 'desc')->first();
					$consumos    = [];
					foreach ($transaccion->detalleTransaccion as $detalletran) {
						//Todo debe modificarse a medida que se vayan agregando productos
						$dproducto = DetalleProdutos::query()
													->where('id', $detalletran->detalle_producto_id)->first();

						if ($dproducto->factura != null) {
							$existe = false;
							if (count($consumos)) {
								foreach ($consumos as $kery => $item) {

									if ($item['servicio'] == 'R') {
										$consumos[$kery]['valor_consumido'] = intval($item['valor_consumido']) + $detalletran->valor;
										$existe                             = true;
									}

								}
							}


							if (!$existe) {
								$data = [
									'servicio'        => 'R',
									'nombre_servicio' => 'Regalo',
									'valor_consumido' => $detalletran->valor,
								];
								array_push($consumos, $data);
							}
						}
						elseif ($dproducto->contrato_emprs_id != null) {
							$existe = false;
							if (count($consumos)) {
								foreach ($consumos as $kery => $item) {

									if ($item['servicio'] == 'B') {
										$consumos[$kery]['valor_consumido'] = intval($item['valor_consumido']) + $detalletran->valor;
										$existe                             = true;
									}

								}
							}
							if (!$existe) {
								$data = [
									'servicio'        => 'B',
									'nombre_servicio' => 'Bono Empresarial',
									'valor_consumido' => $detalletran->valor,
								];
								array_push($consumos, $data);
							}
						}
					}

					$result['estado']             = true;
					$result['numero_transaccion'] = $transaccion->numero_transaccion;
					$result['fecha']              = Carbon::createFromFormat('Y-m-d H:i:s', $transaccion->fecha)->toDateString();
					$result['hora']               = Carbon::createFromFormat('Y-m-d H:i:s', $transaccion->fecha)->toTimeString();
					$result['codigo_terminal']    = $transaccion->codigo_terminal;
					$result['numero_tarjeta']     = $transaccion->numero_tarjeta;
					$result['tipo_transaccion']   = $transaccion->tipo;
					$result['valor']              = $transaccion->valor_count;
					$result['estado']             = $transaccion->htransanccionone;

					$tarjeta = Tarjetas::query()->where('numero_tarjeta', $transaccion->numero_tarjeta)->first();
					if ($tarjeta->persona_id != null) {
						$persona           = Personas::find($tarjeta->persona_id);
						$result['cedula']  = $persona->identificacion;
						$result['nombres'] = $persona->nombres." ".$persona->apellidos;
					}
					else {
						$result['nombres'] = "";
						$result['cedula']  = "";
					}
					$result['consumos'] = $consumos;

					return Response::responseSuccess(MessageResponse::MESSAGE_QUERY_SUCCESS,
						CodesResponse::CODE_OK, $result);
				}
				else {
					return Response::responseError('Debe enviar enviar codigo_terminal',
						CodesResponse::CODE_FORM_INVALIDATE);
				}
			} catch (\Exception $exception) {
				return Response::responseError(MessageResponse::MESSAGE_QUERY_ERROR." => ".$exception->getMessage(),
					CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		public function infoTransaccion(Request $request) {
			try {
				if ($request->has('codigo_terminal')) {
					$transaccion = Transaccion::with('detalleTransaccion', 'htransanccionone')
											  ->withCount([
												  'detalleTransaccion as valor' => function ($query) {
													  $query->select([\DB::raw('SUM(valor)')]);
												  },
											  ])
											  ->where('numero_transaccion', $request->numero_transaccion)
											  ->where('codigo_terminal', $request->codigo_terminal)
											  ->orderBy('fecha', 'desc')->first();
					$consumos    = [];
					foreach ($transaccion->detalleTransaccion as $detalletran) {
						//Todo debe modificarse a medida que se vayan agregando productos
						$dproducto = DetalleProdutos::query()
													->where('id', $detalletran->detalle_producto_id)->first();

						if ($dproducto->factura != null) {
							$existe = false;
							if (count($consumos)) {
								foreach ($consumos as $kery => $item) {

									if ($item['servicio'] == 'R') {
										$consumos[$kery]['valor_consumido'] = intval($item['valor_consumido']) + $detalletran->valor;
										$existe                             = true;
									}

								}
							}


							if (!$existe) {
								$data = [
									'servicio'        => 'R',
									'nombre_servicio' => 'Regalo',
									'valor_consumido' => $detalletran->valor,
								];
								array_push($consumos, $data);
							}
						}
						elseif ($dproducto->contrato_emprs_id != null) {
							$existe = false;
							if (count($consumos)) {
								foreach ($consumos as $kery => $item) {

									if ($item['servicio'] == 'B') {
										$consumos[$kery]['valor_consumido'] = intval($item['valor_consumido']) + $detalletran->valor;
										$existe                             = true;
									}

								}
							}
							if (!$existe) {
								$data = [
									'servicio'        => 'B',
									'nombre_servicio' => 'Bono Empresarial',
									'valor_consumido' => $detalletran->valor,
								];
								array_push($consumos, $data);
							}
						}
					}

					$result['estado']             = true;
					$result['numero_transaccion'] = $transaccion->numero_transaccion;
					$result['fecha']              = Carbon::createFromFormat('Y-m-d H:i:s', $transaccion->fecha)->toDateString();
					$result['hora']               = Carbon::createFromFormat('Y-m-d H:i:s', $transaccion->fecha)->toTimeString();
					$result['codigo_terminal']    = $transaccion->codigo_terminal;
					$result['numero_tarjeta']     = $transaccion->numero_tarjeta;
					$result['tipo_transaccion']   = $transaccion->tipo;
					$result['valor']              = $transaccion->valor_count;
					$result['estado']             = $transaccion->htransanccionone;

					$tarjeta = Tarjetas::query()->where('numero_tarjeta', $transaccion->numero_tarjeta)->first();
					if ($tarjeta->persona_id != null) {
						$persona           = Personas::find($tarjeta->persona_id);
						$result['cedula']  = $persona->identificacion;
						$result['nombres'] = $persona->nombres." ".$persona->apellidos;
					}
					else {
						$result['nombres'] = "";
						$result['cedula']  = "";
					}
					$result['consumos'] = $consumos;

					return Response::responseSuccess(MessageResponse::MESSAGE_QUERY_SUCCESS,
						CodesResponse::CODE_OK, $result);
				}
				else {
					return Response::responseError('Debe enviar enviar codigo_terminal',
						CodesResponse::CODE_FORM_INVALIDATE);
				}
			} catch (\Exception $exception) {
				return Response::responseError(MessageResponse::MESSAGE_QUERY_ERROR." => ".$exception->getMessage(),
					CodesResponse::CODE_INTERNAL_SERVER);
			}
		}
	}
