<?php

	namespace creditocofrem\Http\Controllers;

	use Caffeinated\Shinobi\Facades\Shinobi;
	use creditocofrem\Contratos_empr;
	use creditocofrem\Empresas;
	use creditocofrem\FacturaRegalo;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\Response;
	use creditocofrem\Motivo;
	use creditocofrem\TipoTarjeta;
	use Illuminate\Http\Request;
	use Carbon\Carbon;
	use creditocofrem\Htarjetas;
	use creditocofrem\Tarjetas;
	use creditocofrem\Servicios;
	use creditocofrem\TarjetaServicios;
	use creditocofrem\DetalleProdutos;
	use creditocofrem\Personas;
	use Facades\creditocofrem\Encript;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Storage;
	use Yajra\Datatables\Datatables;
	use creditocofrem\Http\Controllers\TarjetasController;
	use creditocofrem\Transaccion;
	use creditocofrem\HEstadoTransaccion;
	use creditocofrem\DetalleTransaccion;
	use creditocofrem\AdminisTarjetas;
	use creditocofrem\ValorTarjeta;
	use creditocofrem\PagaPlastico;
	use Illuminate\Support\Facades\Auth;

	class TarjetasBonoController extends Controller {

		//Abre el formulario para la creacion individual de tarjetas bono
		public function viewCrearTarjetaIndividual() {
			return view('tarjetas.bono.individualmente');
		}

		//Abre el formulario para la creacion en bloque de tarjetas bono
		public function viewCrearTarjetaBloque() {
			return view('tarjetas.bono.crearbloque');
		}


		/**
		 * funcion crear tarjeta de bono empresarial Individua
		 *
		 *****Condiciones:
		 * Si el contrato existe, registra los valores, si NO, No debe dejar, el usuario debe agregar primero el contrato.
		 * si  el numero de tarjeta existe, omite tarjetas, htarjetas y tarjeta_servicios. Si NO, la inserta en esas tablas
		 * Si  el documento de la persona existe, lo asocia, Si NO, inserta primero la persona (docuemtno, nombres  apellidos, tipo= tercero)
		 ******TABLAS INVOLUCRADAS:
		 * tarjetas
		 * htarjetas
		 * tarjeta_servicios
		 * personas
		 * detalle_produtos
		 * transacciones
		 * detalle_transacciones
		 * hestado_transacciones
		 *
		 * @return array|mixed
		 */
		public function crearTarjetaIndividual(Request $request) {

			//quitamos la mascara de pesos al monto
			$monto = str_replace(".", "", $request->monto);

			$cupo = TarjetasController::validarCupoMinimo(Tarjetas::CODIGO_SERVICIO_BONO);

			if ($cupo["estado"]) {

				if ($monto < $cupo["valor"]) {

					return Response::responseError(
						"El cupo mínimo permitido para el servicio regalo es de $".number_format($cupo["valor"], 0, ',', '.'),
						CodesResponse::CODE_BAD_REQUEST);

				}
			} else {
				return Response::responseError($cupo["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			$tarjeta = Tarjetas::with("persona", "tipo.servicios")->where("numero_tarjeta", $request->numero_tarjeta)->first();


			//validamos que la trajeta que llega exista
			if ($tarjeta == null) {

				return Response::responseNotFound(Tarjetas::$TEXT_RESULT_TARJETA_NO_EXISTE, CodesResponse::CODE_BAD_REQUEST);

			}

			if ($tarjeta->persona != null) {
				if ($tarjeta->persona->identificacion != $request->identificacion) {

					return Response::responseError("Esta tarjeta ya pertenese a otra persona.", CodesResponse::CODE_BAD_REQUEST);

				}
			}

			//validamos que el tipo de la tarjeta este activo para poder continuar
			if ($tarjeta->tipo->estado == TipoTarjeta::ESTADO_INACTIVO) {

				return Response::responseError(Tarjetas::$TEXT_RESULT_TARJETA_TIPO_INACTIVA, CodesResponse::CODE_BAD_REQUEST);

			}

			//validamos que la tarjeta que llega se le pueda asignar el servicio Bono Empresarial
			if (!$tarjeta->tipo->servicios->pluck("codigo")->contains(Tarjetas::CODIGO_SERVICIO_BONO)) {

				return Response::responseError(Tarjetas::$TEXT_RESULT_TARJETA_NO_SERVICIO, CodesResponse::CODE_BAD_REQUEST);

			}

			$banderaPagaPlastico = false;
			$banderaPagoAdmon    = true;
			$valorPlatico        = 0;


			$PagaPlstico = TarjetasController::validarRegaloPagaPlastico(Tarjetas::CODIGO_SERVICIO_BONO);
			if ($PagaPlstico["estado"]) {

				if ($PagaPlstico["paga"]) {
					$banderaPagaPlastico = true;
					$valorPlatico        = $PagaPlstico["valor"];
				}
			} else {
				return Response::responseError($PagaPlstico["mensaje"], CodesResponse::CODE_BAD_REQUEST);

			}

			$contrato = Contratos_empr::with("administracion", "empresa")->where("n_contrato", $request->numero_contrato)->first();

			//TODO validar la factura y ver si esta pagada totalmente

			if ($contrato != null) {

				if ($contrato->forma_pago == Contratos_empr::FORMA_PAGO_EFECTIVO) {
					$tip_docu = $contrato->empresa->tipo_documento;
					$cli_coda = $contrato->empresa->nit;

					$factura = FacturaRegalo::query()->where("cxc_nume", $request->numero_factura)->first();

					if ($factura == null) {
						//Consultamos la factura por el servicio SEVEN
						$resulconsula = TarjetasController::consultarFacturaWS(406, 3928, $tip_docu, $cli_coda, 6064, $contrato->n_factura);

						if (!$resulconsula["estado"]) {
							return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}

						$factura = $resulconsula["factura"];

						if ($factura->cxc_sald != 0) {
							return Response::responseError(
								"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_BONO." a esta tarjeta por que la factura no se ha pagado",
								CodesResponse::CODE_BAD_REQUEST);

						}
					} else {
						if ($factura->cxc_sald != 0) {
							//Consultamos la factura por el servicio SEVEN
							$resulconsula =
								TarjetasController::consultarFacturaWS(406, 3928, $tip_docu, $cli_coda, 6064, $contrato->n_factura);

							if (!$resulconsula["estado"]) {
								return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
							}

							$factura = $resulconsula["factura"];

							if ($factura->cxc_sald != 0) {
								return Response::responseError(
									"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_BONO." a esta tarjeta por que la factura no se ha pagado",
									CodesResponse::CODE_BAD_REQUEST);
							}
						}
					}
				}

				\DB::beginTransaction();

				try {

					//consulta si existe la persona, SiNO, la inserta.
					$persona = Personas::query()->where("identificacion", $request->identificacion)->first();

					if ($persona == null) { //no existe la persona
						$result = $this->crearPersona($request->identificacion, $request->nombres, $request->apellidos);
						if (!$result["estado"]) {
							\DB::rollBack();

							return Response::responseError($result["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}

						$persona = $result["persona"];

					}

					//validar si la suma del monto y cantidad de tarjetas, es igual o menor a la estipulada en el contrato, sino hacer rollback
					$valor_contrato   = $contrato->valor_contrato - $contrato->valor_impuesto;
					$max_num_tarjetas = $contrato->n_tarjetas;

					$detalle = DetalleProdutos::query()->where("contrato_emprs_id", $contrato->id)->get();

					$monto_inicial = $detalle->sum("monto_inicial") + $monto;

					if (count($detalle) > $max_num_tarjetas) {

						return Response::responseError(
							"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_BONO
							." a esta tarjeta, por que supera la cantidad de tarjetas permitidas por el contrado",
							CodesResponse::CODE_BAD_REQUEST);

					}

					if ($monto_inicial <= $valor_contrato) {
						$tarjeta->persona_id = $persona->id;
						$tarjeta->save();

						$valorAdmon = $contrato->administracion->porcentaje;

						$data = $this->crearTarjeteDetalleServicio($tarjeta->numero_tarjeta, $contrato->id, $monto,
							$banderaPagaPlastico, $valorPlatico, $banderaPagoAdmon, $valorAdmon);


						if ($data['estado']) {
							DB::commit();

							$data = $this->crearHtarjeta($tarjeta->id,
								Motivo::CODIGO_M_ASIGNACION_DEL_SERVICIO,
								Tarjetas::CODIGO_SERVICIO_BONO,
								Tarjetas::ESTADO_TARJETA_INACTIVA,
								DetalleProdutos::ESTADO_INACTIVO);
							if (!$data['estado']) {
								\DB::rollBack();

								return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
							}

							return Response::responseSuccess(
								"Un servicio ".Servicios::TEXT_SERVICIO_BONO." se asoció satisfactoriamente a la tarjeta No. ".$tarjeta->numero_tarjeta,
								CodesResponse::CODE_OK,
								null);

						} else {
							\DB::rollBack();

							return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}


					} else {
						\DB::rollBack();

						return Response::responseError(Tarjetas::$TEXT_RESULT_MONTO_SUPERADO, CodesResponse::CODE_BAD_REQUEST);

					}

				} catch (\Exception $exception) {
					\DB::rollBack();

					return Response::responseError('No es posible el servicio '.Servicios::TEXT_SERVICIO_BONO.' '.$exception->getMessage(),
						CodesResponse::CODE_BAD_REQUEST);

				}

			} else {

				return Response::responseError('No es posible asociar el servicio '.Servicios::TEXT_SERVICIO_BONO.', el contrato No. '.$request->numero_contrato
					.' NO Existe',
					CodesResponse::CODE_BAD_REQUEST);

			}

		}

		/**
		 * @param $numero_tarjeta
		 * @param $contrato_id
		 * @param $monto
		 *
		 * @return array|mixed
		 */
		protected function crearTarjeteDetalleServicio($numero_tarjeta,
			$contrato_id,
			$monto,
			$pagaPlastico,
			$valorPlastico,
			$pagoAdmon,
			$valorAdmon) {

			$data                = $this->crearDetalleProducto($numero_tarjeta, $monto, $contrato_id);
			$detalle_producto_id = $data['id'];


			if ($data['estado'] == true) {
				if ($pagaPlastico) {
					$data = $this->transaccionAdminstrativa(
						$numero_tarjeta,
						$valorPlastico,
						$detalle_producto_id,
						DetalleTransaccion::$DESCRIPCION_PLASTICO);
				}
				if ($pagoAdmon) {
					$valorAdministracion = ($monto * $valorAdmon) / 100;
					$data                = $this->transaccionAdminstrativa(
						$numero_tarjeta,
						$valorAdministracion,
						$detalle_producto_id,
						DetalleTransaccion::$DESCRIPCION_ADMINISTRACION);
				}
			}

			return $data;
		}

		/**
		 * Metodo encargado de registrar el detalle del producto a una tarjeta
		 *
		 * @param $numero_tarjeta
		 * @param $monto
		 * @param $contrato_id
		 *
		 * @return array
		 */
		public function crearDetalleProducto($numero_tarjeta, $monto, $contrato_id) {
			try {
				$detalle_producto = new DetalleProdutos();

				$detalle_producto->numero_tarjeta    = $numero_tarjeta;
				$detalle_producto->fecha_cracion     = Carbon::now();
				$detalle_producto->monto_inicial     = $monto;
				$detalle_producto->contrato_emprs_id = $contrato_id;
				$detalle_producto->user_id           = Auth::User()->id;
				$detalle_producto->estado            = DetalleProdutos::ESTADO_INACTIVO;

				$detalle_producto->save();

				$data['estado'] = true;
				$data['id']     = $detalle_producto->id;

			} catch (\Exception $exception) {
				$data['estado']  = false;
				$data['mensaje'] = 'No fue posible crear el detalle_producto para esta tarjeta'.$exception->getMessage();//. $exception->getMessage()
				DB::rollBack();
			}

			return $data;
		}

		private function transaccionAdminstrativa($numero_tarjeta, $valor, $destalle_producto_id, $descripcion) {
			$transaccionAnterior = Transaccion::query()->orderBy("id", "DESC")->first();

			if ($transaccionAnterior != null) {
				$numeroTransaccion = intval($transaccionAnterior->numero_transaccion) + 1;
			} else {
				$numeroTransaccion = 1;
			}

			try {
				$transaccion                     = new Transaccion();
				$transaccion->numero_transaccion = TarjetasController::completarCeros($numeroTransaccion, 10);
				$transaccion->numero_tarjeta     = $numero_tarjeta;
				$transaccion->tipo               = Transaccion::$TIPO_ADMINISTRATIVO;
				//$transaccion->valor = $valor; se elimino ese campo de la base de datos
				$transaccion->fecha = Carbon::now();

				$transaccion->save();

				$hEstado                 = new HEstadoTransaccion();
				$hEstado->transaccion_id = $transaccion->id;
				$hEstado->estado         = HEstadoTransaccion::ESTADO_ACTIVO;
				$hEstado->fecha          = Carbon::now();

				$hEstado->save();

				$detalleTransaccion                      = new DetalleTransaccion();
				$detalleTransaccion->transaccion_id      = $transaccion->id;
				$detalleTransaccion->detalle_producto_id = $destalle_producto_id;
				$detalleTransaccion->valor               = $valor;
				$detalleTransaccion->descripcion         = $descripcion;

				$detalleTransaccion->save();

				$data['estado'] = true;


			} catch (\Exception $e) {
				$data['estado']  = false;
				$data['mensaje'] = 'No fue posible crear la Transaccion '.$e->getMessage();
				DB::rollBack();
			}

			return $data;

		}

		public function crearPersona($iden, $nom, $ape) {
			$result = [];
			try {
				$persona                 = new Personas();
				$persona->identificacion = $iden;
				$persona->nombres        = strtoupper(trim($nom));
				$persona->apellidos      = strtoupper(trim($ape));
				$persona->save();
				$result['estado']  = true;
				$result['persona'] = $persona;
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible crear la persona'.$exception->getMessage();//. $exception->getMessage()
				\DB::rollBack();
			}

			return $result;
		}

		public function crearDetalleProd($num_tarjeta, $monto, $contrato_id, $estado) {
			$result = [];
			try {
				$detalle = DetalleProdutos::query()->where("numero_tarjeta", $num_tarjeta)->where("contrato_emprs_id", $contrato_id)->first();
				if ($detalle != null) {
					$result['estado']  = false;
					$result['mensaje'] = 'Ya existe un detalle de producto asociado al numero de tarjeta y al contrato';//. $exception->getMessage()
					\DB::rollBack();
				} else {
					$detalle                    = new DetalleProdutos();
					$detalle->numero_tarjeta    = $num_tarjeta;
					$detalle->fecha_cracion     = Carbon::now();
					$detalle->monto_inicial     = $monto;
					$detalle->contrato_emprs_id = $contrato_id;
					$detalle->user_id           = \Auth::User()->id;
					$detalle->estado            = $estado;
					$detalle->save();
					$result['estado']     = true;
					$result['mensaje']    = 'El detalle del producto ha sido creado satisfactoriamente';
					$result['detalle_id'] = $detalle->id;
				}
			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible crear el detalle del producto'.$exception->getMessage();//. $exception->getMessage()
				\DB::rollBack();
			}

			return $result;
		}

		public function autoCompleNumContrato(Request $request) {

			$contratos = Contratos_empr::query()->where("n_contrato", "like", "%".$request["query"]."%")->get();
			if (count($contratos) == 0) {
				$data["query"]       = "Unit";
				$data["suggestions"] = [];
			} else {
				$arrayContratos = [];
				foreach ($contratos as $contrato) {
					$arrayContratos[] = [
						"value" => $contrato->n_contrato,
						"data"  => $contrato->id,
					];
				}
				$data["suggestions"] = $arrayContratos;
				$data["query"]       = "Unit";
			}

			return $data;
		}

		public function getNombre(Request $request) {
			$persona = personas::query()->where("identificacion", $request->identificacion)->first();

			return $persona;
		}


		/**Condiciones:
		 * Si el contrato existe, registra los valores, si NO, No debe dejar, el usuario debe agregar primero el contrato.
		 * si  el numero de tarjeta existe, omite tarjetas, htarjetas y tarjeta_servicios. Si NO, la inserta en esas tablas
		 * Si  el documento de la persona existe, lo asocia, Si NO, inserta primero la persona (docuemtno, nombres  apellidos, tipo= tercero)
		 *TABLAS INVOLUCRADAS:
		 * tarjetas
		 * htarjetas
		 * tarjeta_servicios
		 * personas
		 * detalle_produtos
		 * transacciones
		 * detalle_transacciones
		 * hestado_transacciones
		 * PROCESO
		 * valida extension del archivo: txt
		 * sube archivo
		 * lee archivo (linea por linea): formato= ientificacion;nombres;apellidos;monto
		 *  validar cada dato (segun deba ser: numerico, alfabetico etc)
		 * al final: validar que la cantidad de tarjetas asociadas al contrato, no pase el limite inicial, ni el monto total.
		 */
		public function crearTarjetaBloque(Request $request) {

			$primer_num      = $request->numero_tarjeta_inicial;
			$num_contrato    = $request->numero_contrato;

			$contrato = Contratos_empr::with("administracion", "empresa", "detallesproductos")->where("n_contrato", $num_contrato)->first();

			if ($contrato != null) {

				$total_tarjetas  = 0;
				$total_monto     = 0;
				$ruta            = "tmp/";
				$new_name        = $ruta.$primer_num."_".$num_contrato.".txt";
				$fichero         = $request->file('archivo');
				$nombre_file     = $fichero->getClientOriginalName();
				$extensiones     = explode(".", $nombre_file);

				//TODO: no es la mejor opcion de validar el archivo tener cuidado con los otros formatos ya que solo es permitido el TXT
				$extension       = end($extensiones);
				$num_tarjeta_num = $primer_num;
				$anterior        = 1;
				$tarjetaPersona  = [];
				if ($extension == "txt") {
					copy($_FILES['archivo']['tmp_name'], $new_name);
					$contents = [];
					$lineas   = "";
					foreach (file($new_name) as $line) {
						if ($anterior == 1) {
							$contents[]     = $line.PHP_EOL;
							$campos         = explode(";", $line);
							$identificacion = trim($campos[0]);
							$nombres        = trim($campos[1]);
							$apellidos      = trim($campos[2]);
							$monto          = trim($campos[3]);

							if (is_numeric($identificacion) && (strlen($identificacion) > 2 && strlen($identificacion) < 11)) {
								$permitidos = '/^[A-Z üÜáéíóúÁÉÍÓÚñÑ]{1,50}$/i';
								if (preg_match($permitidos, utf8_encode($nombres)) && preg_match($permitidos, utf8_encode($apellidos))) {
									if (is_numeric($monto)) {
										$total_tarjetas++;
										$tarjetaPersona[] =
											["identificacion" => $identificacion, "nombres" => $nombres, "apellidos" => $apellidos, "monto" => $monto];

									} else {
										return Response::responseError(
											'El valor del monto debe ser numérico '.$monto."\nIncorrecta la linea: ".($total_tarjetas + 1)." --- ",
											CodesResponse::CODE_BAD_REQUEST);
									}
								} else {
									return Response::responseError(
										'Los nombres y apellidos deben ser alfabeticos '.$nombres." ".$apellidos."\nIncorrecta la linea: ".($total_tarjetas + 1)
										." --- ",
										CodesResponse::CODE_BAD_REQUEST);
								}

							} else {
								return Response::responseError(
									'El No. de identificación "'.$identificacion.'" debe ser numérico con 3 a 10 digitos '."\nIncorrecta la linea: "
									.($total_tarjetas + 1)." del Archivo ",
									CodesResponse::CODE_BAD_REQUEST);
							}

						}
					}


					$arrayNumTarjetas = [];

					//creamos los numeros de las tarjetas a partir del promer numero ingrasado hasta la cantidad solicitada
					for ($i = $primer_num; $i < ($primer_num + $total_tarjetas); $i++) {

						$arrayNumTarjetas[] = TarjetasController::completarCeros($i);
					}

					//consultamos que los numeros generados anteriormente  exisitan en el invantario de las tarjetas
					$tarjetasExistentes = Tarjetas::query()->whereIn("numero_tarjeta", $arrayNumTarjetas)->pluck("numero_tarjeta");


					// validamos que los numeros generados anteriormente  exisitan en el invantario de las tarjetas
					if (!($tarjetasExistentes->count() == $total_tarjetas)) {

						$tarjetasNoCreadas = collect($arrayNumTarjetas)->diff($tarjetasExistentes);

						if ($tarjetasNoCreadas->count() == 1) {
							$mensajeError = " La tarjeta ".$tarjetasNoCreadas->implode(", ")." NO esta en el inventario";
						} elseif ($tarjetasNoCreadas->count() < 10) {
							$mensajeError = " Las tarjetas ".$tarjetasNoCreadas->implode(", ")." NO estan en el inventario";
						} else {
							$mensajeError = " Las tarjetas ".$tarjetasNoCreadas->slice(1, 9)->implode(", ")."... y otras ".($tarjetasNoCreadas->count() - 9)
								." más, NO estan en el inventario";
						}

						return Response::responseError($mensajeError, CodesResponse::CODE_BAD_REQUEST);

					}

					//consultamos si los numero que exisiten en el vintarios tienen las condiciones para recibir el servicio regalo
					$tarjetasCumplenCondicionBono = Tarjetas::query()
															->whereHas("tipo", function ($query) {
																$query->where("estado", TipoTarjeta::ESTADO_ACTIVO);
															})->whereHas("tipo.servicios", function ($query) {
							$query->where("codigo", Tarjetas::CODIGO_SERVICIO_BONO);
						})->whereIn("numero_tarjeta", $arrayNumTarjetas)
															->pluck("numero_tarjeta");

					//			dd($tarjetasCumplenCondicionBono);
					if (!($tarjetasCumplenCondicionBono->count() == $total_tarjetas)) {

						$tarjetasNoCumplenCondicionBono = collect($arrayNumTarjetas)->diff($tarjetasCumplenCondicionBono);

						if ($tarjetasNoCumplenCondicionBono->count() == 1) {
							$mensajeError =
								" La tarjeta ".$tarjetasNoCumplenCondicionBono->implode(", ")
								." NO cumple con las consicionas para asociarle el servicio regalo";
						} elseif ($tarjetasNoCumplenCondicionBono->count() < 10) {
							$mensajeError =
								" Las tarjetas ".$tarjetasNoCumplenCondicionBono->implode(", ")." NO cumplen con las consicionas para asociarle el servicio "
								.Servicios::TEXT_SERVICIO_BONO;
						} else {
							$mensajeError = " Las tarjetas ".$tarjetasNoCumplenCondicionBono->slice(1, 9)->implode(", ")."... y otras "
								.($tarjetasNoCumplenCondicionBono->count() - 9)
								." más, NO cumplen con las consicionas para asociarle el servicio regalo";
						}

						return Response::responseError($mensajeError, CodesResponse::CODE_BAD_REQUEST);

					}

					$banderaPagaPlastico = false;
					$banderaPagoAdmon    = true;
					$valorPlatico        = 0;

					$PagaPlstico = TarjetasController::validarRegaloPagaPlastico(Tarjetas::CODIGO_SERVICIO_BONO);
					if ($PagaPlstico["estado"]) {

						if ($PagaPlstico["paga"]) {
							$banderaPagaPlastico = true;
							$valorPlatico        = $PagaPlstico["valor"];
						}
					} else {
						return Response::responseError($PagaPlstico["mensaje"], CodesResponse::CODE_BAD_REQUEST);

					}


						if ($contrato->forma_pago == Contratos_empr::FORMA_PAGO_EFECTIVO) {
							$tip_docu = $contrato->empresa->tipo_documento;
							$cli_coda = $contrato->empresa->nit;

							$factura = FacturaRegalo::query()->where("cxc_nume", $request->numero_factura)->first();

							if ($factura == null) {
								//Consultamos la factura por el servicio SEVEN
								$resulconsula = TarjetasController::consultarFacturaWS(406, 3928, $tip_docu, $cli_coda, 6064, $contrato->n_factura);

								if (!$resulconsula["estado"]) {
									return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
								}

								$factura = $resulconsula["factura"];

								if ($factura->cxc_sald != 0) {
									return Response::responseError(
										"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_BONO." a esta tarjeta por que la factura no se ha pagado",
										CodesResponse::CODE_BAD_REQUEST);

								}
							} else {
								if ($factura->cxc_sald != 0) {
									//Consultamos la factura por el servicio SEVEN
									$resulconsula =
										TarjetasController::consultarFacturaWS(406, 3928, $tip_docu, $cli_coda, 6064, $contrato->n_factura);

									if (!$resulconsula["estado"]) {
										return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
									}

									$factura = $resulconsula["factura"];

									if ($factura->cxc_sald != 0) {
										return Response::responseError(
											"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_BONO
											." a esta tarjeta por que la factura no se ha pagado",
											CodesResponse::CODE_BAD_REQUEST);
									}
								}
							}
						}

						//consultar todos los detalles producto que existan relacionados con esta factura para validar el monto valido
						$detalles_producto = $contrato->detallesproductos;

						$tarjetasConEsteContrato = $detalles_producto->whereIn("numero_tarjeta", $arrayNumTarjetas)->pluck("numero_tarjeta");


						//validamos que no existan registros en detalle_producto relacionando con una factura varias veces a la misma tarjeta
						if ($tarjetasConEsteContrato->count() != 0) {


							if ($tarjetasConEsteContrato->count() == 1) {
								$mensajeError =
									" La tarjeta ".$tarjetasConEsteContrato->implode(", ")." ya cuenta con un servicio ".Servicios::TEXT_SERVICIO_BONO
									." asociado a este contrato";
							} elseif ($tarjetasConEsteContrato->count() < 10) {
								$mensajeError =
									" Las tarjetas ".$tarjetasConEsteContrato->implode(", ")." ya cuentan con un servicio ".Servicios::TEXT_SERVICIO_BONO
									." asociado a este contrato";
							} else {
								$mensajeError =
									" Las tarjetas ".$tarjetasConEsteContrato->slice(1, 9)->implode(", ")."... y otras ".($tarjetasConEsteContrato->count() - 9)
									." más, ya cuentan con un servicio ".Servicios::TEXT_SERVICIO_BONO." asociado a este contrato";
							}

							return Response::responseError($mensajeError, CodesResponse::CODE_BAD_REQUEST);

						}

						//validar si la suma del monto y cantidad de tarjetas, es igual o menor a la estipulada en el contrato, sino hacer rollback
						$valor_contrato   = $contrato->valor_contrato - $contrato->valor_impuesto;
						$max_num_tarjetas = $contrato->n_tarjetas;

						$cantidadTarjetasConElContrato = $detalles_producto->count();

						$monto_inicial = $detalles_producto->sum("monto_inicial");


						if (($cantidadTarjetasConElContrato + $total_tarjetas) > $max_num_tarjetas) {

							return Response::responseError(
								"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_BONO
								." a estas tarjetas, por que supera la cantidad de tarjetas permitidas por el contrado",
								CodesResponse::CODE_BAD_REQUEST);

						}

						$collectTarjetaPersona = collect($tarjetaPersona);


						$montoEntrante = $collectTarjetaPersona->sum("monto");


						if (($monto_inicial + $montoEntrante) <= $valor_contrato) {

							foreach ($collectTarjetaPersona as $key => $tarjetaP) {

								//consulta si existe la persona, SiNO, la inserta.
								$tarjetaP = collect($tarjetaP);

								$persona = Personas::query()->where("identificacion", $tarjetaP["identificacion"])->first();

								if ($persona == null) { //no existe la persona
									$result = $this->crearPersona($tarjetaP["identificacion"], $tarjetaP["nombres"], $tarjetaP["apellidos"]);
									if (!$result["estado"]) {
										\DB::rollBack();

										return Response::responseError($result["mensaje"], CodesResponse::CODE_BAD_REQUEST);
									}

									$persona = $result["persona"];

								}

								$tarjeta = Tarjetas::with("persona")->where("numero_tarjeta", $arrayNumTarjetas[$key])->first();

								if ($tarjeta->persona != null) {
									if ($tarjeta->persona->identificacion != $request->identificacion) {
										\DB::rollBack();

										return Response::responseError("Esta tarjeta ya pertenese a otra persona.", CodesResponse::CODE_BAD_REQUEST);

									}
								}

								$tarjeta->persona_id = $persona->id;
								$tarjeta->save();


								$valorAdmon = $contrato->administracion->porcentaje;

								$data = $this->crearTarjeteDetalleServicio($tarjeta->numero_tarjeta, $contrato->id, $monto,
									$banderaPagaPlastico, $valorPlatico, $banderaPagoAdmon, $valorAdmon);

								if (!$data['estado']) {
									\DB::rollBack();

									return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
								}
								$data = $this->crearHtarjeta($tarjeta->id,
									Motivo::CODIGO_M_ASIGNACION_DEL_SERVICIO,
									Tarjetas::CODIGO_SERVICIO_BONO,
									Tarjetas::ESTADO_TARJETA_INACTIVA,
									DetalleProdutos::ESTADO_INACTIVO);
								if (!$data['estado']) {
									\DB::rollBack();

									return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
								}

							}

							DB::commit();

							return Response::responseSuccess(
								"El servicio ".Servicios::TEXT_SERVICIO_BONO." fue asignado satisfactoriamente a  ".$total_tarjetas." Tarjetas",
								CodesResponse::CODE_OK,
								null);

						} else {

							return Response::responseError(Tarjetas::$TEXT_RESULT_MONTO_SUPERADO, CodesResponse::CODE_BAD_REQUEST);

						}

				}else {
					return Response::responseError('No es posible asociar el servicio el fortato del archivo no es el permitido',
						CodesResponse::CODE_BAD_REQUEST);
				}
			} else {
				return Response::responseError('No es posible asociar el servicio '.Servicios::TEXT_SERVICIO_BONO.', el contrato No. '
					.$request->numero_contrato.' NO Existe',
					CodesResponse::CODE_BAD_REQUEST);
			}

		}

		/**
		 *   * metodo que trae la vista para la consulta de servicios de tarjeta bono creadas en el sistema
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function consultaTarjetasBono() {
			return view('tarjetas.bono.consultabono');
		}

		/**
		 * devuelve los datos para mostrar en la grid de los servicios de tarjeta bono que hay en el sistema
		 *
		 * @return mixed
		 * @throws \Exception
		 */
		public function gridConsultaTarjetaBono() {

			$tarjetas = DetalleProdutos::with("tarjeta", "contrato")->where("contrato_emprs_id", "<>", null)->get();


			return Datatables::of($tarjetas)
							 ->addColumn('action', function ($tarjetas) {
								 $acciones = "";
								 $acciones .= '<div class="btn-group">';
								 $acciones .= '<a data-modal href="'.route('gestionarTarjeta', $tarjetas->id)
									 .'" type="button" class="btn btn-custom btn-xs">Gestionar</a>';
								 if (Shinobi::can('editar.fecha.bono')) {
									 $acciones .= '<a data-modal href="'.route('bono.editar', $tarjetas->id)
										 .'" type="button" class="btn btn-custom btn-xs">Editar</a>';
								 }
								 if ($tarjetas->estado == "Inactiva") {
									 $acciones .= '<button type="button" class="btn btn-custom btn-xs" onclick="activar('.$tarjetas->id
										 .')">Activar</button>';
								 }

								 $acciones .= '</div>';

								 return $acciones;
							 })
							 ->make(true);
		}
		//TODO: ES IMPORTANTE ESTO VA SOLO CON PERSONAS QUE TENGA PERMISO PARA HACERLO

		/**
		 * trae la vista del modal para editar una tarjeta bono
		 *
		 * @param $id id del detalle producto de la tarjeta bono a editar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarBono($id) {
			$detalle = DetalleProdutos::query()->find($id);

			return view('tarjetas.bono.modaleditarbono', compact('detalle'));
		}

		/**
		 * metodo que permite editar una tarjeta bono, aunque solo permite editar su fecha de vencimiento
		 *
		 * @param Request $request
		 * @param         $id
		 *
		 * @return array
		 */
		public function editarBono(Request $request, $id) {

			$detalle         = DetalleProdutos::with("contrato")->find($id);
			$monto           = str_replace(".", "", $request->monto_inicial);
			$detalles        = DetalleProdutos::query()->where("contrato_emprs_id", $detalle->contrato_emprs_id)->get();
			$valor_servicios = $detalles->sum("monto_inicial");

			if ((($valor_servicios - $detalle->monto_inicial) + $monto) > $detalle->contrato->valor_contrato){

				return Response::responseError('Monto superado '
					, CodesResponse::CODE_BAD_REQUEST);
			}

			DB::beginTransaction();
			try {

				$detalle->monto_inicial = $monto;
				$fechav                 = $request->fecha_vencimiento;
				list($dia, $mes, $ano) = explode("/", $fechav);
				$fechav                     = $ano."/".$mes."/".$dia;
				$detalle->fecha_vencimiento = $fechav;
				$detalle->save();
				$detalle_trasacion = DetalleTransaccion::query()->where('detalle_producto_id', $detalle->id)
													   ->where('descripcion', DetalleTransaccion::$DESCRIPCION_ADMINISTRACION)
													   ->first();
				if ($detalle_trasacion != null) {
					$administracion           = AdminisTarjetas::query()->where('servicio_codigo', Tarjetas::CODIGO_SERVICIO_BONO)
															   ->where('estado', AdminisTarjetas::ESTADO_ACTIVO)
															   ->first();
					$valorAdministracion      = ($monto * $administracion->porcentaje) / 100;
					$detalle_trasacion->valor = $valorAdministracion;
					$detalle_trasacion->save();
				}
				DB::commit();

				return Response::responseSuccess(
					'Actualizado satisfactoriamente',
					CodesResponse::CODE_OK,
					null);
			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('No fue posible realizar la actualizacion '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);
			}

		}

		/**
		 * metodo que permite activar una tarjeta bono en el sistema
		 *
		 * @param Request $request
		 *
		 * @return array
		 */
		public function activarTarjetaBono(Request $request) {
			$result = [];
			DB::beginTransaction();
			try {
				$detalle                    = DetalleProdutos::query()->find($request->id);
				$detalle->fecha_activacion  = Carbon::now();
				$detalle->fecha_vencimiento = Carbon::now()->addYear();
				$detalle->estado            = DetalleProdutos::ESTADO_ACTIVO;
				$detalle->save();
				$tarjeta         = Tarjetas::query()->where('numero_tarjeta', $detalle->numero_tarjeta)->first();
				$tarjeta->estado = Tarjetas::ESTADO_TARJETA_ACTIVA;
				$tarjeta->save();
				$data = $this->crearHtarjeta(
					$tarjeta->id,
					Motivo::CODIGO_M_ACTIVACION_DEL_SERVICIO,
					"",
					Tarjetas::ESTADO_TARJETA_ACTIVA);

				if (!$data['estado']) {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				$data = $this->crearHtarjeta($detalle->tarjeta->id,
					Motivo::CODIGO_M_ACTIVACION_DEL_SERVICIO,
					Tarjetas::CODIGO_SERVICIO_BONO,
					Tarjetas::ESTADO_TARJETA_ACTIVA,
					DetalleProdutos::ESTADO_ACTIVO
				);
				if (!$data['estado']) {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				DB::commit();

				return Response::responseSuccess('La tarjeta ha sido activada satisfactoriamente.', CodesResponse::CODE_OK, null);
			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('No fue posible activar la tarjeta '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);
			}
		}

		/**
		 *   * metodo que trae la vista para la consulta inteligente de tarjeta bono: filtro por numero de contrato o nit de la empresa,
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewConsultaxContrato() {
			return view('tarjetas.bono.consultabono_xfiltro');
		}

		/**
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 * Funcion consulta por contrato, busca las tarjetas bono (detalles del producto), que corresponden a un numero de contrato
		 */
		public function ConsultaxContrato(Request $request) {
			$contrato = Contratos_empr::where("n_contrato", $request->numcontrato)->first();
			if ($contrato != null) {
				$detalles = DetalleProdutos::where("contrato_emprs_id", $contrato->id)->get();

				return view('tarjetas.bono.parcialconsultaxcontrato', compact('detalles', 'contrato'));
			} else {
				return "<p align='center'>No se encontraron resultados</p>";
			}
		}

		/**
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 * Funcion consulta por contrato, busca los contratos correspondientes a una empresa (busca por nit)
		 */
		public function ConsultaxEmpresa(Request $request) {
			$empresa = Empresas::where("nit", $request->nit)->first();
			//dd($contratos);
			if ($empresa != null) {
				$contratos = Contratos_empr::where("empresa_id", $empresa->id)->get();

				return view('tarjetas.bono.parcialconsultaxempresa', compact('contratos'));
			} else {
				return "<p align='center'>No se encontraron resultados</p>";
			}
		}

		/**
		 * FUNCION ACTIVAR TARJETAS BONO MASIVAMENTE: por el numero de contrato
		 *  *TABLAS INVOLUCRADAS:
		 * tarjetas: si el estado no es activo, lo actualiza
		 * htarjetas: si el detalle_producto no estaba activo, registra la novedad
		 * tarjeta_servicios: si el estado no es activo, lo actualiza
		 * detalle_produtos: si el estado no es activo, lo actualiza
		 */
		public function ActivarxContrato(Request $request) {
			$result = [];

			$contrato = Contratos_empr::query()->where("n_contrato", $request->ncontrato)->first();
			if ($contrato != null) {
				DB::beginTransaction();
				try {
					$detalles          = DetalleProdutos::query()->where('contrato_emprs_id', $contrato->id)->get();
					$fecha_activacion  = Carbon::now();
					$fecha_vencimiento = Carbon::now()->addYear();
					$actual_inactivas  = 0;
					foreach ($detalles as $detalle) {
						if ($detalle->estado != DetalleProdutos::ESTADO_ACTIVO) {
							$actual_inactivas++;
							DetalleProdutos::query()->where('id', $detalle->id)->update([
								'estado'            => DetalleProdutos::ESTADO_ACTIVO, 'fecha_activacion' => $fecha_activacion,
								'fecha_vencimiento' => $fecha_vencimiento,
							]);
							$tarjeta = Tarjetas::query()->where("numero_tarjeta", $detalle->numero_tarjeta)->first();
							if ($tarjeta->estado != Tarjetas::ESTADO_TARJETA_ACTIVA) {
								Tarjetas::query()->where('numero_tarjeta', $detalle->numero_tarjeta)->update([
									'estado' => Tarjetas::ESTADO_TARJETA_ACTIVA,
								]);
								$result['estado'] = true;
							}

							$data = $this->crearHtarjeta(
								$tarjeta->id,
								Motivo::CODIGO_M_ACTIVACION_DEL_SERVICIO,
								"",
								Tarjetas::ESTADO_TARJETA_ACTIVA);

							if (!$data['estado']) {
								\DB::rollBack();

								return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
							}

							$data = $this->crearHtarjeta($detalle->tarjeta->id,
								Motivo::CODIGO_M_ACTIVACION_DEL_SERVICIO,
								Tarjetas::CODIGO_SERVICIO_BONO,
								Tarjetas::ESTADO_TARJETA_ACTIVA,
								DetalleProdutos::ESTADO_ACTIVO
							);
							if (!$data['estado']) {
								\DB::rollBack();

								return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
							}
						}
					}
					if ($actual_inactivas == 0) {

						DB::rollBack();

						return Response::responseError('Los productos ya se encuentran activos', CodesResponse::CODE_BAD_REQUEST);
					} else {

						DB::commit();

						return Response::responseSuccess('Las tarjetas han sido activadas', CodesResponse::CODE_OK, null);
					}
				} catch (\Exception $exception) {
					DB::rollBack();

					return Response::responseError('No fue posible activar las tarjetas bono '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);
				}
			} else {
				//return "<p align='center'>No se encontraron resultados</p>";
				return Response::responseError('No hay tarjetas para activar', CodesResponse::CODE_BAD_REQUEST);

			}

		}


	}
