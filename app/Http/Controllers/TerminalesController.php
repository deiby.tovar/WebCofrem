<?php

	namespace creditocofrem\Http\Controllers;

	use creditocofrem\Establecimientos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Sucursales;
	use creditocofrem\Terminales;
	use creditocofrem\TrasladoTerminal;
	use Facades\creditocofrem\Encript;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Symfony\Component\Console\Terminal;
	use Yajra\Datatables\Datatables;
	use Caffeinated\Shinobi\Facades\Shinobi;

	class TerminalesController extends Controller {

		/**
		 * metodo que me trae la vista donde se me listan las terminales de una sucursal
		 *
		 * @param $id id de la sucursal
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
		 */
		public function index($id) {
			try {
				//consultamos los datos de la sucursal a la cual se le quiere agregar terminales
				$sucursal = Sucursales::query()->findOrFail($id);

				return view('establecimientos.terminales.listaterminales', compact('sucursal'));
			} catch (ModelNotFoundException $exception) {
				//wn caso de que no se encuentre la sucursal retornaremos la pagina de error 404
				return abort(CodesResponse::CODE_NOT_FOUND);
			}

		}

		/**
		 * metodo que me carga el datatable que me lista las terminales de una sucursal
		 *
		 * @param Request $request trae la id de la sucursal
		 *
		 * @return mixed
		 */
		public function gridTerminales(Request $request) {

			//se consultan todas las terminales de una sucursal para ser enviados al datatable del cliente
			$terminales = Terminales::query()->where('sucursal_id', $request->id)->get();

			return Datatables::of($terminales)
							 ->addColumn('action', function ($terminales) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<a href="'.route("terminal.editar", ["id" => $terminales->id])
									 .'" data-modal="modal-lg" class="btn btn-xs btn-custom" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar" ><i class="ti-pencil-alt"></i> </a>';
								 if (Shinobi::can('estado.terminal')) {
									 if ($terminales->estado == Terminales::$ESTADO_TERMINAL_ACTIVA) {
										 $acciones = $acciones.'<bottom class="btn btn-xs btn-danger" onclick="cambiarEstado('.$terminales->id
											 .')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Inactivar"><i class="mdi mdi-close-circle"></i> </bottom>';
									 }
									 else {
										 $acciones = $acciones.'<bottom class="btn btn-xs btn-primary" onclick="cambiarEstado('.$terminales->id
											 .')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Activar"><i class="mdi mdi-checkbox-marked-circle"></i> </bottom>';
									 }
								 }
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * metodo que me muestra el modal para agregar una nueva terminal
		 *
		 * @param Request $request trae la id de la sucursal para saber a cual agregar una nueva terminal
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearTerminal(Request $request) {
			$sucursal_id = $request->id;

			return view('establecimientos.terminales.modalcrearterminal', compact('sucursal_id'));
		}

		/**
		 * metodo que crea una nueva terminal en una sucursal
		 *
		 * @param Request $request datos de la terminal a crear
		 *
		 * @return array
		 */
		public function crearTerminal(Request $request) {
			try {
				//validamos que el numero de activo no exista en a base de dstos
				$validator = \Validator::make($request->all(), [
					'numero_activo' => 'required|unique:terminales',
				]);

				if ($validator->fails()) {//si el numero de activo existe retornaos un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}

				// se busca cual es el ultimo codigo de terminales que se a creado para asignar el siguiente
				$ultimaTerminal = Terminales::select([\DB::raw('max(codigo) as codigo')])->first();
				if ($ultimaTerminal->codigo == null) {// de no existir nungun codigo en el sistema se asigna el 1
					$codigo = '000000000000001';
				}
				else {
					//de existir el codigo, lo pasamos a un etero, le sumamos uno y finalmente rellenamos con cero de ser necesario
					$codigo = intval($ultimaTerminal->codigo);
					$codigo++;
					$largo = strlen($codigo);
					for ($i = 0; $i < (15 - $largo); $i++) {
						$codigo = "0".$codigo;
					}
				}
				$terminal = new Terminales(); // se crea una nueva instacia del modelo terminales
				/*inicializamos los atributos del modelo terminal para luego guardarlos en base de datos */
				$terminal->codigo        = $codigo;
				$terminal->numero_activo = $request->numero_activo;
				$terminal->celular       = $request->celular;
				$terminal->password      = Encript::encryption($request->password);//encriptamos la contraseña de la terminal
				$terminal->sucursal_id   = $request->getQueryString(); //asociamos la terminal a la sucursal
				$sucursal                = Sucursales::find($request->getQueryString()); //consultamos la sucursal para determinar su estado
				if ($sucursal->estado == Sucursales::INACTIVA) //si la sucursal esta inactiva, la terminal tambien se crea en estado inactiva
				{
					$terminal->estado = Terminales::$ESTADO_TERMINAL_INACTIVA;
				}
				$terminal->save();//guardamos los cambios en la base de datos

				//retornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $terminal,
				], CodesResponse::CODE_CREATED);

			} catch (\Exception $exception) {
				//de ocurrir algun error retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * trae el modal para editar una terminal
		 *
		 * @param Request $request la id de la terminal que se quiere editar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarTerminal(Request $request) {
			try {
				//se consulta la termina que se quiere editar
				$terminal = Terminales::query()->findOrFail($request->id);

				//retornamos la vista al modal que cargara el formulario de editar terminal
				return view('establecimientos.terminales.modaleditarterminal', compact('terminal'));
			} catch (ModelNotFoundException $exception) {
				//en caso de no existir la terminal retornamos un error 404 al cliente
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}
		}


		/**
		 * metodo usado para editar una terminal especifica
		 *
		 * @param Request $request id de la terminal que se quiere editar
		 *
		 * @return array
		 */
		public function editarTerminal(Request $request) {

			try {
				//consultamos la termina que se desea editar
				$terminal = Terminales::query()->findOrFail($request->getQueryString());

				//verificamos que el numero de activo no exista en la base de datos, solo para elcaso que se quiera cambiar denuemo de activo
				$validator = \Validator::make($request->all(), [
					'numero_activo' => 'required|unique:terminales,numero_activo,'.$terminal->id,
				]);

				if ($validator->fails()) {//si el numero de activo existe retornamos un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}

				//actualizamos los atributos de la terminal
				$terminal->numero_activo = $request->numero_activo;
				$terminal->celular       = $request->celular;
				if (trim($request->password) != "") {//si enviamos una contrase, la actualizamos
					$terminal->password = Encript::encryption(trim($request->password));
				}

				$terminal->save();// guardamos los cambios en la base de datos

				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_SUCCESS,
					KeysResponse::KEY_DATA    => $terminal,
				], CodesResponse::CODE_OK);

			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * metodo que cambia el estado de una terminal especifica
		 *
		 * @param Request $request trae la id de la terminal que se desea cambiar de estado
		 *
		 * @return array
		 */
		public function cambiarEstadoTerminal(Request $request) {
			try {
				//se consultada la terminal a la cual se desea cambiar de estado
				$terminal = Terminales::with('getSucursal')->findOrFail($request->id);

				if ($terminal->estado == Terminales::$ESTADO_TERMINAL_ACTIVA) {//si la terminal tenia estado activa la cambiamos a inactiva
					$terminal->estado = Terminales::$ESTADO_TERMINAL_INACTIVA;
				}
				else {
					//si la termina esta en estado inactiva, se consulta si la sucursal esta en estado activa
					if ($terminal->getSucursal->estado == Sucursales::ACTIVA) {
						//de estar la sucursal activa, se permite el cambio de la terminal a activa
						$terminal->estado = Terminales::$ESTADO_TERMINAL_ACTIVA;
					}
					else {
						//de la sucursal estar inactiva, se envia un mensaje de error al cliente, indicando que no se puede activar la terminal
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => 'No se puede activar una terminal de una sucursal inactiva',
							KeysResponse::KEY_DATA    => null,
						], CodesResponse::CODE_BAD_REQUEST);
					}

				}
				$terminal->save();// se guardan los cambios en base de datos
				// se envia un mensaje de respuesta positivo al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_SUCCESS,
					KeysResponse::KEY_DATA    => $terminal,
				], CodesResponse::CODE_OK);

			} catch (\Exception $exception) {
				//de ocurrir un erro se envia el mensaje al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}

		}

		/**
		 * trae la vista de la lista de todas las teminales en el sistema
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewListTerminalTraslados() {
			$establecimientos    = Establecimientos::query()->where('estado', Establecimientos::ACTIVA)->get();
			$sucursales          = Sucursales::query()->where('estado', Sucursales::ACTIVA)->get();
			$terminalesActivas   = Terminales::query()->where('estado', Terminales::$ESTADO_TERMINAL_ACTIVA)->get();
			$terminalesInactivas = Terminales::query()->where('estado', Terminales::$ESTADO_TERMINAL_INACTIVA)->get();

			return view('establecimientos.terminales.trasladoterminal',
				compact('establecimientos',
					'sucursales',
					'terminalesActivas',
					'terminalesInactivas'
				));
		}

		/**
		 * metodo que carga la grid con todas las terminales de la red cofrem
		 *
		 * @return mixed
		 */
		public function gridTerminalesTraslado() {
			$terminales = Terminales::with("getSucursal", "getSucursal.getEstablecimiento")->get();

			return Datatables::of($terminales)
							 ->addColumn('action', function ($terminales) {
								 $accion = '';
								 $accion .= '<a href="'.route('viewtrasladoterminal', $terminales->id)
									 .'" data-modal="" class="btn btn-custom btn-xs"><i class="fa fa-exchange" aria-hidden="true"></i> Trasladar</a>';
								 $accion .= '<a href="'.route('viewhistorialtraslados', $terminales->id)
									 .'" data-modal="modal-lg" class="btn btn-info btn-xs"><i class="ti-pulse" aria-hidden="true"></i> Historial</a>';

								 return $accion;
							 })->make(true);
		}

		/**
		 * metodo que trae la vista del modal para selecionar a donde se va a trasladar la terminal
		 *
		 * @param $id id correspondiente a la terminal que se va a trasladar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewTrasladoTerminal($id) {
			$terminal         = Terminales::query()->find($id);
			$establecimientos = Establecimientos::query()
												//->where('estado', Establecimientos::ACTIVA) //TODO considerar si es necesario que este activo el establecimiento
												->pluck('razon_social', 'id');

			return view('establecimientos.terminales.modaltrasladoterminal', compact('terminal', 'establecimientos'));
		}

		/**
		 * metodo usado para traer la vista del historial de traslados de una terminal
		 *
		 * @param $id
		 */
		public function viewHistorialTraslados($id) {
			try {
				$terminal = Terminales::query()->findOrFail($id);

				return view('establecimientos.terminales.modalhostorialtraslados', compact('terminal'));
			} catch (\Exception $exception) {
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}

		}


		public function gridTerminalesHistorial($id)
		{
			$traslados = TrasladoTerminal::with('origen.getEstablecimiento','destino.getEstablecimiento')->where('terminal_id',$id)->get();
			return Datatables::of($traslados)
							 ->make(true);
		}

		/**
		 * metodo usuado para crear un traslado a una terminal
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function trasladarTerminal(Request $request) {

			DB::beginTransaction(); //inciamos una transacion en la base de datos para evitar datos huerfanos
			try {
				$terminal = Terminales::with('getSucursal')->find($request->getQueryString()); // consultamos la terminal a trasladar

				if ($terminal->sucursal_id != $request->sucursal_id) { // validamos que la sucursal de destino sea diferente a la de origen

					//creamos una historial de traslado para la terminal
					$terminal->traslados()->create([
						'origen_id'  => $terminal->sucursal_id,
						'destino_id' => $request->sucursal_id,
					]);

					//actualizamos el valor dela sucursal en la terminal
					$terminal->sucursal_id = $request->sucursal_id;
					$terminal->save();//Guardamos los cambios de la terminal
					DB::commit(); //guardamos los cambios en la base de datos
					//retornamos una respuesta positiva al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
						KeysResponse::KEY_DATA    => $terminal,
					], CodesResponse::CODE_CREATED);
				}
				else {
					//si la sucursal origien es igual a la destino, enviamos un error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => 'La terminal ya pertenece a esta sucursal',
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				DB::rollBack(); // en caso de algun error descartamos los cambios en la base de datos
				//enviamos una respuesta del error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}
	}
