<?php

	namespace creditocofrem\Http\Controllers;

	use Carbon\Carbon;
	use creditocofrem\Htarjetas;
	use creditocofrem\Motivo;
	use Illuminate\Foundation\Bus\DispatchesJobs;
	use Illuminate\Routing\Controller as BaseController;
	use Illuminate\Foundation\Validation\ValidatesRequests;
	use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
	use Illuminate\Support\Facades\Auth;

	class Controller extends BaseController {

		use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


		/**
		 * Metodo encargado de insertar los registros del historico de las tarjetas
		 *
		 * @param $tarjetas_id
		 * @param $codigo_motivo
		 * @param $servicio_codigo
		 * @param $estado_tarjeta
		 * @param $estado_producto
		 * @param $nota
		 *
		 * @return array
		 */
		public function crearHtarjeta($tarjetas_id, $codigo_motivo, $servicio_codigo, $estado_tarjeta, $estado_producto = null, $nota = null) {
			$result = [];

			try {

				$motivo = Motivo::query()->where("codigo", $codigo_motivo)->first();

				$htarjetas = new Htarjetas();

				$htarjetas->motivo_id       = $motivo->id;
				$htarjetas->estado          = $estado_tarjeta;
				$htarjetas->estado_producto = $estado_producto;
				$htarjetas->fecha           = Carbon::now();
				$htarjetas->tarjetas_id     = $tarjetas_id;
				$htarjetas->user_id         = Auth::check()?auth()->User()->id:1;
				$htarjetas->servicio_codigo = $servicio_codigo;
				$htarjetas->nota = $nota;
				$htarjetas->save();
				$result['estado']  = true;
				$result['mensaje'] = 'El historial de tarjeta ha sido creado satisfactoriamente';


			} catch (\Exception $exception) {
				$result['estado']  = false;
				$result['mensaje'] = 'No fue posible crear el historial de tarjeta'.$exception->getMessage();//. $exception->getMessage()
				\DB::rollBack();
			}

			return $result;
		}


	}
