<?php

	namespace creditocofrem\Http\Controllers;

	use Caffeinated\Shinobi\Facades\Shinobi;
	use creditocofrem\AdminisTarjetas;
	use creditocofrem\CupoMinimo;
	use creditocofrem\DetalleProdutos;
	use creditocofrem\DetalleTransaccion;
	use creditocofrem\FacturaRegalo;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\Response;
	use creditocofrem\HEstadoTransaccion;
	use creditocofrem\Motivo;
	use creditocofrem\PagaPlastico;
	use creditocofrem\Servicios;
	use creditocofrem\Tarjetas;
	use creditocofrem\Htarjetas;
	use creditocofrem\TipoDocumento;
	use creditocofrem\TipoTarjeta;
	use creditocofrem\Transaccion;
	use creditocofrem\ValorTarjeta;
	use Carbon\Carbon;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use SoapClient;
	use SoapFault;
	use Yajra\Datatables\Datatables;


	class TarjetasRegaloController extends Controller {

		/**
		 * metodo encargado de mostrar la vista principal para crear las Tarjetas
		 * Regalo
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function crearTarjetaRegalo() {

			$tipoDocumentos = TipoDocumento::all();

			return view("tarjetas.regalo.individualmente", compact("tipoDocumentos"));
		}

		/**
		 * metodo encargado de mostrar la vista principal para crear las Tarjetas en Bloque
		 * Regalo
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function crearTarjetaBloque() {

			$tipoDocumentos = TipoDocumento::all();

			return view("tarjetas.regalo.bloque", compact("tipoDocumentos"));
		}

		/**
		 * Metodo encargado de autocompletar las tarjetas buscadas por el número de
		 * tarjeta
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return mixed
		 */
		public function autoCompleNumTarjeta(Request $request) {

			if ($request->has('codigo')) {
				$tarjetas = Tarjetas::query()->whereHas("tipo.servicios", function ($q) use ($request) {
					$q->where('codigo', $request->codigo);
				})->where("numero_tarjeta", "like", "%".$request["query"]."%")->get();
			} else {
				$tarjetas = Tarjetas::query()->where("numero_tarjeta", "like", "%".$request["query"]."%")->get();
			}

			if (count($tarjetas) == 0) {
				$data["query"]       = "Unit";
				$data["suggestions"] = [];
			} else {
				$arrayTarjetas = [];
				foreach ($tarjetas as $producto) {
					$arrayTarjetas[] = [
						"value" => $producto->numero_tarjeta,
						"data"  => $producto->id,
					];
				}
				$data["suggestions"] = $arrayTarjetas;
				$data["query"]       = "Unit";
			}

			return $data;
		}

		/**
		 * Metodo para agregar las Tarjetas Retgalo
		 *
		 * @param Request $request
		 *
		 * @return array
		 */
		public function addTarjetaRegalo(Request $request) {

			//quitamos la mascara de pesos al monto
			$monto = str_replace(".", "", $request->monto);

			$cupo = TarjetasController::validarCupoMinimo(Tarjetas::CODIGO_SERVICIO_REGALO);
			if ($cupo["estado"]) {

				if ($monto < $cupo["valor"]) {
					return Response::responseError("El cupo mínimo permitido para el servicio ".Servicios::TEXT_SERVICIO_REGALO." es de $"
						.number_format($cupo["valor"], 0, ',', '.')
						, CodesResponse::CODE_BAD_REQUEST);

				}
			} else {
				return Response::responseError($cupo["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			$tarjeta = Tarjetas::with("tipo.servicios")->where("numero_tarjeta", $request->numero_tarjeta)->first();

			//validamos que la trajeta que llega exista
			if ($tarjeta == null) {
				return Response::responseNotFound(Tarjetas::$TEXT_RESULT_TARJETA_NO_EXISTE, CodesResponse::CODE_BAD_REQUEST);

			}

			//validamos que el tipo de la tarjeta este activo para poder continuar
			if ($tarjeta->tipo->estado == TipoTarjeta::ESTADO_INACTIVO) {
				return Response::responseError(Tarjetas::$TEXT_RESULT_TARJETA_TIPO_INACTIVA, CodesResponse::CODE_BAD_REQUEST);

			}

			//validamos que la tarjeta que llega se le pueda asignar el servicio regalo
			if (!$tarjeta->tipo->servicios->pluck("codigo")->contains(Tarjetas::CODIGO_SERVICIO_REGALO)) {
				return Response::responseError(Tarjetas::$TEXT_RESULT_TARJETA_NO_SERVICIO, CodesResponse::CODE_BAD_REQUEST);

			}

			$banderaPagaPlastico = false;
			$banderaPagoAdmon    = false;
			$valorPlatico        = 0;
			$valorAdmon          = 0;

			$PagaPlstico = TarjetasController::validarRegaloPagaPlastico(Tarjetas::CODIGO_SERVICIO_REGALO);
			if ($PagaPlstico["estado"]) {

				if ($PagaPlstico["paga"]) {
					$banderaPagaPlastico = true;
					$valorPlatico        = $PagaPlstico["valor"];
				}
			} else {

				return Response::responseError($PagaPlstico["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			$PagaAdmon = TarjetasController::validarRegaloPagaAdmon(Tarjetas::CODIGO_SERVICIO_REGALO);

			if ($PagaAdmon["estado"]) {

				if ($PagaAdmon["paga"]) {
					$banderaPagoAdmon = true;
					$valorAdmon       = $PagaAdmon["valor"];
				}
			} else {
				return Response::responseError($PagaAdmon["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			//consultar todos los detalles producto que existan relacionados con esta factura para validar el monto valido
			$detalles_producto = DetalleProdutos::query()->where("factura", $request->numero_factura)->get();

			//			dd($detalles_producto->sum("monto_inicial"));

			//validamos que no existan registros en detalle_producto relacionando con una factura varias veces a la misma tarjeta
			if ($detalles_producto->where("numero_tarjeta", $request->numero_tarjeta)->count() == 0) {

				$factura = FacturaRegalo::query()->where("cxc_nume", $request->numero_factura)->first();

				if ($factura == null) {
					//Consultamos la factura por el servicio SEVEN
					$resulconsula = TarjetasController::consultarFacturaWS(406, 3927, $request->tip_docu, $request->cli_coda, 6064, $request->numero_factura);

					if (!$resulconsula["estado"]) {
						return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
					}

					$factura = $resulconsula["factura"];

					if ($factura->cxc_sald != 0) {
						return Response::responseError(
							"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_REGALO." a esta tarjeta por que la factura no se ha pagado",
							CodesResponse::CODE_BAD_REQUEST);
					}
				} else {
					if ($factura->cxc_sald != 0) {
						//Consultamos la factura por el servicio SEVEN
						$resulconsula =
							TarjetasController::consultarFacturaWS(406, 3927, $request->tip_docu, $request->cli_coda, 6064, $request->numero_factura);

						if (!$resulconsula["estado"]) {
							return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}

						$factura = $resulconsula["factura"];

						if ($factura->cxc_sald != 0) {
							return Response::responseError(
								"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_REGALO." a esta tarjeta por que la factura no se ha pagado",
								CodesResponse::CODE_BAD_REQUEST);
						}
					}
				}

				$monto_inicial = $detalles_producto->sum("monto_inicial") + $monto;

				//validamos si los montos que estan en los detalle_producto mas el monto de la tarjeta que se esta tratando de crear es permitido
				if ($monto_inicial <= $factura->cxc_tota) {


					DB::beginTransaction();

					try {

						$data = $this->crearTarjeteDetalleServicio($request->numero_tarjeta, $request->numero_factura, $monto,
							$banderaPagaPlastico, $valorPlatico, $banderaPagoAdmon, $valorAdmon);

						if ($data['estado']) {
							DB::commit();


							if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_CREADA) {

								$data = $this->crearHtarjeta($tarjeta->id, Motivo::CODIGO_M_INICIALIZACION_DE_SERVICIO, "",Tarjetas::ESTADO_TARJETA_INACTIVA);

								if (!$data['estado']) {
									\DB::rollBack();

									return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
								}
								$tarjeta->estado = Tarjetas::ESTADO_TARJETA_INACTIVA;
								$tarjeta->save();
							}

							$data = $this->crearHtarjeta($tarjeta->id,
								Motivo::CODIGO_M_ASIGNACION_DEL_SERVICIO,
								Tarjetas::CODIGO_SERVICIO_REGALO,
								Tarjetas::ESTADO_TARJETA_INACTIVA,
								DetalleProdutos::ESTADO_INACTIVO
							);
							if (!$data['estado']) {
								\DB::rollBack();

								return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
							}


							return Response::responseSuccess(
								"Un servicio ".Servicios::TEXT_SERVICIO_REGALO." se asoció satisfactoriamente a la tarjeta No. ".$tarjeta->numero_tarjeta,
								CodesResponse::CODE_OK,
								null);

						} else {
							DB::rollBack();

							return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}
					} catch (\Exception $exception) {
						DB::rollBack();

						return Response::responseError('No es posible el servicio '.Servicios::TEXT_SERVICIO_BONO.' '.$exception->getMessage(),
							CodesResponse::CODE_BAD_REQUEST);

					}

				} else {
					return Response::responseError(Tarjetas::$TEXT_RESULT_MONTO_SUPERADO, CodesResponse::CODE_BAD_REQUEST);

				}

			} else {
				return Response::responseError(Tarjetas::$TEXT_RESULT_FACTURA_Y_NUMTARJETA_EXISTEN, CodesResponse::CODE_BAD_REQUEST);

			}

		}

		public function addTarjetaRegaloBloque(Request $request) {

			//quitamos la mascara de pesos al monto
			$monto = str_replace(".", "", $request->monto);

			$cupo = TarjetasController::validarCupoMinimo(Tarjetas::CODIGO_SERVICIO_REGALO);
			if ($cupo["estado"]) {

				if ($monto < $cupo["valor"]) {
					return Response::responseError("El cupo mínimo permitido para el servicio ".Servicios::TEXT_SERVICIO_REGALO." es de $"
						.number_format($cupo["valor"], 0, ',', '.')
						, CodesResponse::CODE_BAD_REQUEST);
				}
			} else {
				return Response::responseError($cupo["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			$data       = [];
			$total      = $request->cantidad;
			$primer_num = $request->numero_tarjeta_inicial;

			$arrayNumTarjetas = [];

			//creamos los numeros de las tarjetas a partir del promer numero ingrasado hasta la cantidad solicitada
			for ($i = $primer_num; $i < ($primer_num + $total); $i++) {

				$arrayNumTarjetas[] = TarjetasController::completarCeros($i);
			}

			//consultamos que los numeros generados anteriormente  exisitan en el invantario de las tarjetas
			$tarjetasExistentes = Tarjetas::query()->whereIn("numero_tarjeta", $arrayNumTarjetas)->pluck("numero_tarjeta");


			// validamos que los numeros generados anteriormente  exisitan en el invantario de las tarjetas
			if (!($tarjetasExistentes->count() == $total)) {

				$tarjetasNoCreadas = collect($arrayNumTarjetas)->diff($tarjetasExistentes);

				if ($tarjetasNoCreadas->count() == 1) {
					$mensajeError = " La tarjeta ".$tarjetasNoCreadas->implode(", ")." NO esta en el inventario";
				} elseif ($tarjetasNoCreadas->count() < 10) {
					$mensajeError = " Las tarjetas ".$tarjetasNoCreadas->implode(", ")." NO estan en el inventario";
				} else {
					$mensajeError = " Las tarjetas ".$tarjetasNoCreadas->slice(1, 9)->implode(", ")."... y otras ".($tarjetasNoCreadas->count() - 9)
						." más, NO estan en el inventario";
				}

				return Response::responseError($mensajeError, CodesResponse::CODE_BAD_REQUEST);

			}

			//consultamos si los numero que exisiten en el vintarios tienen las condiciones para recibir el servicio regalo
			$tarjetasCumplenCondicionRegalo = Tarjetas::query()
													  ->whereHas("tipo", function ($query) {
														  $query->where("estado", TipoTarjeta::ESTADO_ACTIVO);
													  })->whereHas("tipo.servicios", function ($query) {
					$query->where("codigo", Tarjetas::CODIGO_SERVICIO_REGALO);
				})->whereIn("numero_tarjeta", $arrayNumTarjetas)
													  ->pluck("numero_tarjeta");

			//			dd($tarjetasCumplenCondicionRegalo);
			if (!($tarjetasCumplenCondicionRegalo->count() == $total)) {

				$tarjetasNoCumplenCondicionRegalo = collect($arrayNumTarjetas)->diff($tarjetasCumplenCondicionRegalo);

				if ($tarjetasNoCumplenCondicionRegalo->count() == 1) {
					$mensajeError =
						" La tarjeta ".$tarjetasNoCumplenCondicionRegalo->implode(", ")." NO cumple con las consicionas para asociarle el servicio "
						.Servicios::TEXT_SERVICIO_REGALO;
				} elseif ($tarjetasNoCumplenCondicionRegalo->count() < 10) {
					$mensajeError =
						" Las tarjetas ".$tarjetasNoCumplenCondicionRegalo->implode(", ")." NO cumplen con las consicionas para asociarle el servicio "
						.Servicios::TEXT_SERVICIO_REGALO;
				} else {
					$mensajeError = " Las tarjetas ".$tarjetasNoCumplenCondicionRegalo->slice(1, 9)->implode(", ")."... y otras "
						.($tarjetasNoCumplenCondicionRegalo->count() - 9)
						." más, NO cumplen con las consicionas para asociarle el servicio regalo";
				}

				return Response::responseError($mensajeError, CodesResponse::CODE_BAD_REQUEST);

			}

			$banderaPagaPlastico = false;
			$banderaPagoAdmon    = false;
			$valorPlatico        = 0;
			$valorAdmon          = 0;

			$PagaPlstico = TarjetasController::validarRegaloPagaPlastico(Tarjetas::CODIGO_SERVICIO_REGALO);
			if ($PagaPlstico["estado"]) {

				if ($PagaPlstico["paga"]) {
					$banderaPagaPlastico = true;
					$valorPlatico        = $PagaPlstico["valor"];
				}
			} else {
				return Response::responseError($PagaPlstico["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}

			$PagaAdmon = TarjetasController::validarRegaloPagaAdmon(Tarjetas::CODIGO_SERVICIO_REGALO);

			if ($PagaAdmon["estado"]) {

				if ($PagaAdmon["paga"]) {
					$banderaPagoAdmon = true;
					$valorAdmon       = $PagaAdmon["valor"];
				}
			} else {
				return Response::responseError($PagaAdmon["mensaje"], CodesResponse::CODE_BAD_REQUEST);
			}


			//consultar todos los detalles producto que existan relacionados con esta factura para validar el monto valido
			$detalles_producto = DetalleProdutos::query()->where("factura", $request->numero_factura)->get();

			$montoTotalBloque = $request->cantidad * $monto;

			$montoInicialTotal = $montoTotalBloque + $detalles_producto->sum("monto_inicial");

			$tarjetasConEstaFactura = $detalles_producto->whereIn("numero_tarjeta", $arrayNumTarjetas)->pluck("numero_tarjeta");


			//validamos que no existan registros en detalle_producto relacionando con una factura varias veces a la misma tarjeta
			if ($tarjetasConEstaFactura->count() != 0) {


				if ($tarjetasConEstaFactura->count() == 1) {
					$mensajeError = " La tarjeta ".$tarjetasConEstaFactura->implode(", ")." ya cuenta con un servicio ".Servicios::TEXT_SERVICIO_REGALO
						." asociado a esta factura";
				} elseif ($tarjetasConEstaFactura->count() < 10) {
					$mensajeError = " Las tarjetas ".$tarjetasConEstaFactura->implode(", ")." ya cuentan con un servicio ".Servicios::TEXT_SERVICIO_REGALO
						." asociado a esta factura";
				} else {
					$mensajeError = " Las tarjetas ".$tarjetasConEstaFactura->slice(1, 9)->implode(", ")."... y otras ".($tarjetasConEstaFactura->count() - 9)
						." más, ya cuentan con un servicio ".Servicios::TEXT_SERVICIO_REGALO." asociado a esta factura";
				}

				return Response::responseError($mensajeError, CodesResponse::CODE_BAD_REQUEST);

			}

			$factura = FacturaRegalo::query()->where("cxc_nume", $request->numero_factura)->first();

			if ($factura == null) {
				//Consultamos la factura por el servicio SEVEN
				$resulconsula = TarjetasController::consultarFacturaWS(406, 3927, $request->tip_docu, $request->cli_coda, 6064, $request->numero_factura);

				if (!$resulconsula["estado"]) {
					return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				$factura = $resulconsula["factura"];

				if ($factura->cxc_sald != 0) {
					return Response::responseError(
						"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_REGALO." a esta tarjeta por que la factura no se ha pagado",
						CodesResponse::CODE_BAD_REQUEST);
				}
			} else {
				if ($factura->cxc_sald != 0) {
					//Consultamos la factura por el servicio SEVEN
					$resulconsula = TarjetasController::consultarFacturaWS(406, 3927, $request->tip_docu, $request->cli_coda, 6064, $request->numero_factura);

					if (!$resulconsula["estado"]) {
						return Response::responseError($resulconsula["mensaje"], CodesResponse::CODE_BAD_REQUEST);
					}

					$factura = $resulconsula["factura"];

					if ($factura->cxc_sald != 0) {
						return Response::responseError(
							"NO se puede asignar el servicio ".Servicios::TEXT_SERVICIO_REGALO." a esta tarjeta por que la factura no se ha pagado",
							CodesResponse::CODE_BAD_REQUEST);
					}
				}
			}

			//validamos si los montos que estan en los detalle_producto mas el monto de la tarjeta que se esta tratando de crear es permitido
			if ($montoInicialTotal <= $factura->cxc_tota) {


				DB::beginTransaction();

				try {

					foreach ($arrayNumTarjetas as $numTarjeta) {

						$tarjeta = Tarjetas::query()->where("numero_tarjeta", $numTarjeta)->first();

						$data = $this->crearTarjeteDetalleServicio($numTarjeta, $request->numero_factura, $monto,
							$banderaPagaPlastico, $valorPlatico, $banderaPagoAdmon, $valorAdmon);

						if (!$data['estado']) {
							DB::rollBack();

							return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}

						if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_CREADA) {

							$data = $this->crearHtarjeta($tarjeta->id, Motivo::CODIGO_M_INICIALIZACION_DE_SERVICIO, "",Tarjetas::ESTADO_TARJETA_INACTIVA);

							if (!$data['estado']) {
								\DB::rollBack();

								return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
							}
							$tarjeta->estado = Tarjetas::ESTADO_TARJETA_INACTIVA;
							$tarjeta->save();
						}

						$data = $this->crearHtarjeta($tarjeta->id,
							Motivo::CODIGO_M_ASIGNACION_DEL_SERVICIO,
							Tarjetas::CODIGO_SERVICIO_REGALO,
							Tarjetas::ESTADO_TARJETA_INACTIVA,
							DetalleProdutos::ESTADO_INACTIVO);
						if (!$data['estado']) {
							\DB::rollBack();

							return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
						}

					}
					DB::commit();

					return Response::responseSuccess(
						"El servicio ".Servicios::TEXT_SERVICIO_REGALO." fue asignado satisfactoriamente a  ".$total." Tarjetas",
						CodesResponse::CODE_OK,
						null);

				} catch (\Exception $exception) {
					DB::rollBack();

					return Response::responseError('No es posible asignar el servicio '.Servicios::TEXT_SERVICIO_REGALO.' a estas tarjetas por: '
						.$exception->getMessage(),
						CodesResponse::CODE_BAD_REQUEST);

				}

			} else {
				return Response::responseError(Tarjetas::$TEXT_RESULT_MONTO_SUPERADO,
					CodesResponse::CODE_BAD_REQUEST);
			}

		}


		private function transaccionAdminstrativa($numero_tarjeta, $valor, $destalle_producto_id, $descripcion) {
			$transaccionAnterior = Transaccion::query()->orderBy("id", "DESC")->first();

			if ($transaccionAnterior != null) {
				$numeroTransaccion = intval($transaccionAnterior->numero_transaccion) + 1;
			} else {
				$numeroTransaccion = 1;
			}


			try {
				$transaccion                     = new Transaccion();
				$transaccion->numero_transaccion = TarjetasController::completarCeros($numeroTransaccion, 10);
				$transaccion->numero_tarjeta     = $numero_tarjeta;
				$transaccion->tipo               = Transaccion::$TIPO_ADMINISTRATIVO;
				//$transaccion->valor = $valor; se elimino ese campo de la base de datos
				$transaccion->fecha = Carbon::now();

				$transaccion->save();

				$hEstado                 = new HEstadoTransaccion();
				$hEstado->transaccion_id = $transaccion->id;
				$hEstado->estado         = HEstadoTransaccion::ESTADO_ACTIVO;
				$hEstado->fecha          = Carbon::now();

				$hEstado->save();

				$detalleTransaccion                      = new DetalleTransaccion();
				$detalleTransaccion->transaccion_id      = $transaccion->id;
				$detalleTransaccion->detalle_producto_id = $destalle_producto_id;
				$detalleTransaccion->valor               = $valor;
				$detalleTransaccion->descripcion         = $descripcion;

				$detalleTransaccion->save();

				$data['estado'] = true;


			} catch (\Exception $e) {
				$data['estado']  = false;
				$data['mensaje'] = 'No fue posible crear la Transaccion '.$e->getMessage();
				DB::rollBack();
			}

			return $data;

		}

		/**
		 * @param $numero_tarjeta
		 * @param $numero_factura
		 * @param $monto
		 *
		 * @return array|mixed
		 */
		private function crearTarjeteDetalleServicio($numero_tarjeta,
			$numero_factura,
			$monto,
			$pagaPlastico,
			$valorPlastico,
			$pagoAdmon,
			$valorAdmon) {

			$data                = $this->crearDetalleProducto($numero_tarjeta, $monto, $numero_factura);
			$detalle_producto_id = $data['id'];


			if ($data['estado'] == true) {
				if ($pagaPlastico) {
					$data = $this->transaccionAdminstrativa(
						$numero_tarjeta,
						$valorPlastico,
						$detalle_producto_id,
						DetalleTransaccion::$DESCRIPCION_PLASTICO);
				}
				if ($pagoAdmon) {
					$valorAdministracion = ($monto * $valorAdmon) / 100;
					$data                = $this->transaccionAdminstrativa(
						$numero_tarjeta,
						$valorAdministracion,
						$detalle_producto_id,
						DetalleTransaccion::$DESCRIPCION_ADMINISTRACION);
				}
			}

			return $data;
		}

		/**
		 * Metodo encargado de registrar el detalle del producto a una tarjeta
		 *
		 * @param $numero_tarjeta
		 * @param $monto
		 * @param $factura
		 *
		 * @return array
		 */
		private function crearDetalleProducto($numero_tarjeta, $monto, $factura) {
			try {
				$detalle_producto = new DetalleProdutos();

				$detalle_producto->numero_tarjeta = $numero_tarjeta;
				$detalle_producto->fecha_cracion  = Carbon::now();
				$detalle_producto->monto_inicial  = $monto;
				$detalle_producto->factura        = $factura;
				$detalle_producto->user_id        = Auth::User()->id;
				$detalle_producto->estado         = DetalleProdutos::ESTADO_INACTIVO;

				$detalle_producto->save();

				$data['estado'] = true;
				$data['id']     = $detalle_producto->id;

			} catch (\Exception $exception) {
				$data['estado']  = false;
				$data['mensaje'] = 'No fue posible crear el detalle_producto para esta tarjeta'.$exception->getMessage();//. $exception->getMessage()
				DB::rollBack();
			}

			return $data;
		}

		/**
		 * metodo que trae la vista para la consulta de servicios de tarjeta regalo creadas en el sistema
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function consultaTarjetasRegalo() {
			return view('tarjetas.regalo.consultaregalo');
		}

		/**
		 * devuelve los datos para mostrar en la grid de los servicios de tarjeta regalo que hay en el sistema
		 *
		 * @return mixed
		 * @throws \Exception
		 */
		public function gridConsulaTarjetaRegalo() {

			$detalleRegalo = DetalleProdutos::with("tarjeta")->where("factura", "<>", null)->get();

			return Datatables::of($detalleRegalo)
							 ->addColumn('action', function ($detalleRegalo) {
								 $acciones = "";
								 $acciones .= '<div class="btn-group">';
								 $acciones .= '<a data-modal href="'.route('gestionarTarjeta', $detalleRegalo->id)
									 .'" type="button" class="btn btn-custom btn-xs">Gestionar</a>';
								 if (Shinobi::can('editar.monto.regalo')) {
									 $acciones .= '<a data-modal href="'.route('regalo.editar', $detalleRegalo->id)
										 .'" type="button" class="btn btn-custom btn-xs">Editar</a>';
								 }
								 if ($detalleRegalo->estado == "Inactiva") {
									 $acciones .= '<button type="button" class="btn btn-custom btn-xs" onclick="activar('.$detalleRegalo->id
										 .')">Activar</button>';
								 }
								 $acciones .= '</div>';

								 return $acciones;
							 })
							 ->make(true);
		}


		//TODO: ES IMPORTANTE ESTO VA SOLO CON PERSONAS QUE TENGA PERMISO PARA HACERLO

		/**
		 * trae la vista del modal para editar una tarjeta regalo
		 *
		 * @param $id id del detalle producto de la tarjeta regalo a editar
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarRegalo($id) {

			$detalle = DetalleProdutos::query()->find($id);

			return view('tarjetas.regalo.modaleditarregalo', compact('detalle'));
		}

		/**
		 * metodo que permite editar una tarjeta regalo, aunque solo permite editar su monto
		 *
		 * @param Request $request
		 * @param         $id
		 *
		 * @return array
		 */
		public function editarRegalo(Request $request, $id) {
			$result = [];
			DB::beginTransaction();

			try {

				//quitamos la mascara de pesos al monto
				$monto = str_replace(".", "", $request->monto_inicial);

				$detalle = DetalleProdutos::query()->find($id);

				//consultar todos los detalles producto que existan relacionados con esta factura para validar el monto valido
				$detalles_producto = DetalleProdutos::query()->where("factura", $detalle->factura)->get();

				$montoInicialTotal = $monto + $detalles_producto->sum("monto_inicial");


				$factura = FacturaRegalo::query()->where("cxc_nume", $detalle->factura)->first();

				if ($factura == null) {
					//Consultamos la factura por el servicio SEVEN
					$resulconsula = $this->consultarFacturaWS($request->tip_docu, $request->cli_coda, $request->numero_factura);

					if (!$resulconsula["estado"]) {
						return $resulconsula;
					}
					$factura = $resulconsula["factura"];
				}

				//validamos si los montos que estan en los detalle_producto mas el monto de la tarjeta que se esta tratando de crear es permitido
				if ($montoInicialTotal <= $factura->cxc_tota) {

					$detalle->monto_inicial = $monto;
					$detalle->save();
					$detalle_trasacion = DetalleTransaccion::query()->where('detalle_producto_id', $detalle->id)
														   ->where('descripcion', DetalleTransaccion::$DESCRIPCION_ADMINISTRACION)
														   ->first();
					if ($detalle_trasacion != null) {
						$administracion           = AdminisTarjetas::query()->where('servicio_codigo', Tarjetas::CODIGO_SERVICIO_REGALO)
																   ->where('estado', AdminisTarjetas::ESTADO_ACTIVO)
																   ->first();
						$valorAdministracion      = $detalle->monto_inicial * ($administracion->porcentace / 100);
						$detalle_trasacion->valor = $valorAdministracion;
						$detalle_trasacion->save();
					}
					DB::commit();

					return Response::responseSuccess('Actualizado satisfactoriamente', CodesResponse::CODE_OK, null);

				} else {
					return Response::responseError(Tarjetas::$TEXT_RESULT_MONTO_SUPERADO, CodesResponse::CODE_BAD_REQUEST);

				}

			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('No fue posible realizar la actualizacion '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

			}

		}


		/**
		 * metodo que permite activar una tarjeta regalo en el sistema
		 *
		 * @param Request $request
		 *
		 * @return array
		 */
		public function activarTarjetaRegalo(Request $request) {
			$result = [];
			DB::beginTransaction();
			try {
				$detalle = DetalleProdutos::with("tarjeta")->find($request->id);

				$detalle->fecha_activacion  = Carbon::now();
				$detalle->fecha_vencimiento = Carbon::now()->addYear();
				$detalle->estado            = DetalleProdutos::ESTADO_ACTIVO;
				$detalle->save();
				$detalle->tarjeta()->update(["estado" => Tarjetas::ESTADO_TARJETA_ACTIVA]);


				$data = $this->crearHtarjeta($detalle->tarjeta->id, Motivo::CODIGO_M_ACTIVACION_DE_SERVICIOS, "",Tarjetas::ESTADO_TARJETA_ACTIVA);

				if (!$data['estado']) {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				$data = $this->crearHtarjeta($detalle->tarjeta->id,
					Motivo::CODIGO_M_ACTIVACION_DEL_SERVICIO,
					Tarjetas::CODIGO_SERVICIO_REGALO,
					Tarjetas::ESTADO_TARJETA_ACTIVA,
					DetalleProdutos::ESTADO_ACTIVO
				);
				if (!$data['estado']) {
					\DB::rollBack();

					return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
				}

				DB::commit();

				return Response::responseSuccess('La tarjeta ha sido activada satisfactoriamente.', CodesResponse::CODE_OK, null);

			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('No fue posible activar la tarjeta '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

			}

		}


		/**
		 * metodo que trae la vista para la consulta de servicios de tarjeta regalo creadas en el sistema
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function consultaTarjetasRegaloAvanzada() {
			return view('tarjetas.regalo.consultafactura');
		}

		public function restadoConsultaxFacturaRegalo(Request $request) {

			$factura = $request->factura;

			$tarjetas = Tarjetas::with("detallep")->whereHas("detallep", function ($query) use ($factura) {
				$query->where("factura", $factura);
			})->get();

			//				dd($tarjetas);

			return view('tarjetas.regalo.parcialconsultaxfactura', compact('tarjetas', 'factura'));
		}

		public function activarxFacturaRegalo(Request $request) {
			$result = [];
			DB::beginTransaction();
			try {
				$detallesproductos = DetalleProdutos::query()
													->where('factura', $request->factura)
													->where('estado', "<>", DetalleProdutos::ESTADO_ACTIVO)
													->get();

				if ($detallesproductos->count() == 0) {
					return Response::responseError('Los productos ya se encuentran activos', CodesResponse::CODE_BAD_REQUEST);
				}

				foreach ($detallesproductos as $detalle) {

					$detalle->fecha_activacion  = Carbon::now();
					$detalle->fecha_vencimiento = Carbon::now()->addYear();
					$detalle->estado            = DetalleProdutos::ESTADO_ACTIVO;
					$detalle->save();
					$tarjeta         = Tarjetas::query()->where('numero_tarjeta', $detalle->numero_tarjeta)->first();
					$tarjeta->estado = Tarjetas::ESTADO_TARJETA_ACTIVA;
					$tarjeta->save();

					$data = $this->crearHtarjeta(
						$tarjeta->id,
						Motivo::CODIGO_M_ACTIVACION_DE_SERVICIOS,
						"",
						Tarjetas::ESTADO_TARJETA_ACTIVA);

					if (!$data['estado']) {
						\DB::rollBack();

						return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
					}

					$data = $this->crearHtarjeta($tarjeta->id,
						Motivo::CODIGO_M_ACTIVACION_DEL_SERVICIO,
						Tarjetas::CODIGO_SERVICIO_REGALO,
						Tarjetas::ESTADO_TARJETA_ACTIVA,
						DetalleProdutos::ESTADO_ACTIVO
					);
					if (!$data['estado']) {
						\DB::rollBack();

						return Response::responseError($data["mensaje"], CodesResponse::CODE_BAD_REQUEST);
					}

				}

				DB::commit();

				return Response::responseSuccess('Las tarjetas han sido activadas', CodesResponse::CODE_OK, null);

			} catch (\Exception $exception) {
				DB::rollBack();

				return Response::responseError('No fue posible activar la tarjeta '.$exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);
			}

		}
	}
