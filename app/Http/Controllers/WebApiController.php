<?php

	namespace creditocofrem\Http\Controllers;


	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\DetalleProdutos;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Motivo;
	use creditocofrem\Personas;
	use creditocofrem\Tarjetas;
	use creditocofrem\TarjetaServicios;
	use creditocofrem\Terminales;
	use Facades\creditocofrem\Encript;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	//use Facades\creditocofrem\AESCrypt;
	use creditocofrem\ApiWS;
	use creditocofrem\Transaccion;
	use creditocofrem\DetalleTransaccion;
	use creditocofrem\HEstadoTransaccion;
	use creditocofrem\Htarjetas;
	use Carbon\Carbon;
	use creditocofrem\Duplicado;
	use creditocofrem\DuplicadoProductos;

	class WebApiController extends Controller {

		//

		/**
		 * valida que las terminales esten cominicadas
		 *
		 * @param Request $request
		 * - codigo
		 *
		 * @return mixed
		 */
		public function comunicacion(Request $request) {
			try {
				if ($request->codigo == '02') {
					$result[KeysResponse::KEY_STATUS]  = KeysResponse::STATUS_SUCCESS;
					$result[KeysResponse::KEY_MESSAGE] = ApiWS::$TEXT_COMUNICACION_TEST_EXITOSA;

					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_COMUNICACION_TEST_EXITOSA,
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_OK);
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_COMUNICACION_TEST_ERROR,
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}

		}

		/**
		 * valida la terminal
		 *
		 * @param Request $request
		 * -codigo
		 *
		 * @return array
		 */
		public function validarTerminal(Request $request) {
			try {
				$terminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($terminal != null) {
					$sucursal                = $terminal->getSucursal;
					$establecimiento         = $sucursal->getEstablecimiento;
					$data['nit']             = $establecimiento->nit;
					$data['razon_social']    = $establecimiento->razon_social;
					$data['sucursal_id']     = $sucursal->id;
					$data['nombre_sucursal'] = $sucursal->nombre;
					$data['ciudad']          = $sucursal->getMunicipio->descripcion;
					$data['direccion']       = $sucursal->direccion;
					$data['estado_sucursal'] = $sucursal->estado;
					$data['estado_terminal'] = $terminal->estado;
					$data['codigo_terminal'] = $terminal->codigo;
					$data['ip1']             = "190.159.199.209";

					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_VALIDACION_EXITOSA,
						KeysResponse::KEY_DATA    => $data,
					], CodesResponse::CODE_OK);
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_BAD_REQUEST);

				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_OPERACION,
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * asigana los datos faltantes de la terminal
		 *
		 * @param Request $request
		 * -codigo
		 * -uuid
		 * -mac
		 * -imei
		 *
		 * @return array
		 */
		public function asignaID(Request $request) {
			DB::beginTransaction();
			try {
				$terminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($terminal != null) {
					$terminal->imei = $request->imei;
					$terminal->uid  = $request->uuid;
					$terminal->mac  = $request->mac;
					$terminal->save();
					DB::commit();

					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_VALIDACION_EXITOSA,
						KeysResponse::KEY_DATA    => $terminal,
					], CodesResponse::CODE_OK);

				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => $terminal,
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				DB::rollBack();

				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_OPERACION,
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * permite validar la clave de la terminal, esto para caso de configuracion del dispositivo
		 *
		 * @param Request $request
		 * -codigo
		 * -password
		 *
		 * @return array
		 * -retirna array con codigos de error, y mensaje
		 */
		public function validarClaveTerminal(Request $request) {
			try {
				$teminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($teminal != null) {
					$contrasena = Encript::decryption($teminal->password);
					if ($request->password == $contrasena) {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_CORRECTO,
							KeysResponse::KEY_DATA    => $contrasena,
						], CodesResponse::CODE_OK);
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
							KeysResponse::KEY_DATA    => null,
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * valida que la clave de la sucursal, esot para casos administrativos (anulacion etc)
		 *
		 * @param Request $request
		 * -codigo
		 * -password
		 *
		 * @return array
		 */
		public function validarClaveSucursal(Request $request) {
			try {
				$teminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($teminal != null) {
					$sucursal   = $teminal->getSucursal;
					$contrasena = Encript::decryption($sucursal->password);
					if ($request->password == $contrasena) {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_CORRECTO,
							KeysResponse::KEY_DATA    => $contrasena,
						], CodesResponse::CODE_OK);
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
							KeysResponse::KEY_DATA    => null,
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => null,
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * Metodo encargado de retornas los servicios activos asociados a una tarjeta
		 *
		 * @param \Illuminate\Http\Request $request
		 * - codigo
		 * - numero_tarjeta
		 * - identificacion
		 *
		 * @return array
		 */
		public function getServicios(Request $request) {
			try {
				$terminal = Terminales::query()->where('codigo', $request->codigo)->exists();
				if ($terminal) {
					$tarjeta = Tarjetas::query()->where('numero_tarjeta', $request->numero_tarjeta)->first();
					if ($tarjeta != null) {
						if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {
							if ($tarjeta->cambioclave == 1) {
								if ($tarjeta->persona_id != null) {
									$persona = Personas::query()->where('identificacion', $request->identificacion)->exists();
									if ($persona) {
										$result = $this->retornaServicios($tarjeta, $request);

										return response()->json($result, CodesResponse::CODE_OK);
									}
									else {
										return response()->json([
											KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
											KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_DOCUMENTIO_INCORRECTO,
											KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_DOCUMENTIO_INCORRECTO],
										], CodesResponse::CODE_BAD_REQUEST);
									}
								}
								else {
									$result = $this->retornaServicios($tarjeta, $request);

									return response()->json($result, CodesResponse::CODE_OK);
								}
							}
							else {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_CAMBIO_CLAVE,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_CAMBIO_CLAVE],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
						else {
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
								KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_NO_VALIDA,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_NO_VALIDA],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_NO_EXISTE],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);

			}
		}

		/**
		 * retorna los servicios de la tarjeta
		 *
		 * @param $tarjeta
		 * @param $request
		 *
		 * @return array
		 */
		private function retornaServicios($tarjeta, $request) {

			$numero_tarjeta = $request->numero_tarjeta;
			$resultado      = [];

			$detalles = DetalleProdutos::query()->where('numero_tarjeta', $numero_tarjeta)
									   ->where('estado', DetalleProdutos::ESTADO_ACTIVO)
									   ->get();
			foreach ($detalles as $detalle) {
				$gasto          = 0;
				$dtransacciones = DetalleTransaccion::query()->where('detalle_producto_id', $detalle->id)->get();//$detalle->id
				foreach ($dtransacciones as $dtransaccione) {
					$htransaccion =
						DB::table('h_estado_transacciones')->where('transaccion_id', $dtransaccione->transaccion_id)->orderBy('id', 'desc')->first();
					if ($htransaccion->estado == HEstadoTransaccion::ESTADO_ACTIVO) {
						$gasto += $dtransaccione->valor;
					}
				}
				$sobrante = $detalle->monto_inicial - $gasto;
				//$sobrante = number_format($sobrante, 0, ',', '.');
				if ($detalle->factura != null) {
					$codigo_servicio = Tarjetas::CODIGO_SERVICIO_REGALO;
					$servicio        = 'Regalo';
				}
				else {
					$codigo_servicio = Tarjetas::CODIGO_SERVICIO_BONO;
					$servicio        = 'Bono empresarial';
				}


				$existe = false;
				if (count($resultado)) {
					foreach ($resultado as $kery => $item) {

						if ($item['codigo_servicio'] == $codigo_servicio) {
							//dd(intval($item['valor']));
							$resultado[$kery]['saldo'] = intval($item['saldo']) + $sobrante;
							$existe                    = true;
						}

					}
				}

				if (!$existe) {

					$resultado[] = [
						'codigo_servicio' => $codigo_servicio,
						'descripcion'     => $servicio,
						'saldo'           => $sobrante,
					];
				}

			}

			for ($i = 0; $i < count($resultado); $i++) {
				$resultado[$i]['saldo'] = number_format($resultado[$i]['saldo'], 0, ',', '.');
			}


			$numeroDeRegistros = count($resultado);

			$result = [
				KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
				KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TRANSACCION_EXITOSA,
				KeysResponse::KEY_DATA    => ['servicios' => $resultado, 'numero_de_registros' => $numeroDeRegistros],
			];

			return $result;
		}

		/**
		 * metodo usado para validar la clave de la tarjeta
		 *
		 * @param Request $request
		 * -numero_tarjeta
		 * -password
		 *
		 * @return array
		 */
		public function consultarClaveTarjeta(Request $request) {
			try {
				$tarjeta = Tarjetas::query()->where('numero_tarjeta', $request->numero_tarjeta)->first();
				if ($tarjeta != null) {
					if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {
						if ($tarjeta->persona_id != null) {
							$persona = Personas::query()->where('identificacion', $request->identificacion)->exists();
							if ($persona) {
								if ($request->password == Encript::decryption($tarjeta->password)) {
									return response()->json([
										KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
										KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_VALIDACION_EXITOSA,
										KeysResponse::KEY_DATA    => null,
									], CodesResponse::CODE_OK);
								}
								else {
									return response()->json([
										KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
										KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
										KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_INCORECTO],
									], CodesResponse::CODE_BAD_REQUEST);
								}
							}
							else {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_DOCUMENTIO_INCORRECTO,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_DOCUMENTIO_INCORRECTO],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
						else {
							if ($request->password == Encript::decryption($tarjeta->password)) {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_VALIDACION_EXITOSA,
									KeysResponse::KEY_DATA    => null,
								], CodesResponse::CODE_OK);
							}
							else {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_INCORECTO],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_NO_VALIDA,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_NO_VALIDA],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_BAD_REQUEST);
			}
		}

		/**
		 * permite actualizar el la contraseña de la tarjeta
		 *
		 * @param Request $request
		 * -numero_tarjeta
		 * -password
		 * -nuevo_password
		 *
		 * @return array
		 */
		public function actulizarClaveTarjeta(Request $request) {
			try {
				$tarjeta = Tarjetas::query()->where('numero_tarjeta', $request->numero_tarjeta)->first();
				if ($tarjeta != null) {
					if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {
						if ($request->password == Encript::decryption($tarjeta->password)) {
							if (is_numeric($request->nuevo_password)) {
								$tarjeta->password    = Encript::encryption(trim($request->nuevo_password));
								$tarjeta->cambioclave = 1;
								$tarjeta->save();

								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TRANSACCION_EXITOSA,
									KeysResponse::KEY_DATA    => $tarjeta,
								], CodesResponse::CODE_OK);
							}
							else {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_DEBE_SER_NUM,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_DEBE_SER_NUM],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
						else {
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
								KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_INCORECTO],
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_NO_VALIDA,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_NO_VALIDA],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_BAD_REQUEST);
			}
		}

		/**
		 *
		 * @param \Illuminate\Http\Request $request
		 * -codigo
		 * -numero_tarjeta
		 * -valor
		 * -password
		 * -servicios
		 *
		 * @return array
		 */
		public function consumo(Request $request) {
			$result = [];
			\DB::beginTransaction();//inicamos una transacion en base de datos para evitar datos huerfanos
			$consumus = [];
			try {
				$valorConsumir = $request->valor; //obtenemos el valor a consumir en la transaccion
				//consultamos la terminal que envia la peticion de transaccion
				$terminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($terminal != null) {//si la terminal existe.
					if ($terminal->estado == Terminales::$TERMINAL_ESTADO_ACTIVA) {//si la terminal esta activa
						//consultamos la tarjeta, a la cual se le va a generar el consumo
						$tarjeta = Tarjetas::query()->where('numero_tarjeta', $request->numero_tarjeta)->first();
						if ($tarjeta != null) {//si existe la tarjeta
							if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {//si la tarjeta esta activa
								if ($request->password
									== Encript::decryption($tarjeta->password)) { //si la contraseña enviada por el request coincide con la que esta en el sistema
									$servicios      = explode(',', $request->servicios);// obtenemos los servicios que se van a consumir
									$newTransaccion = new Transaccion();//creamos una nueva transaccion
									//obtenemos la ultima transaccion creada en el sistema
									$ultimaTransaccion = Transaccion::select([\DB::raw('max(numero_transaccion) as numero')])->first();
									if ($ultimaTransaccion->numero == null) {//si no existe transaciones
										$numero = '0000000001';//creamos la primera transaccion
									}
									else {//existe transaccion, obtenemos el numero de la tranasacion
										$numero = intval($ultimaTransaccion->numero);
										$numero++;
										$largo = strlen($numero);
										for ($i = 0; $i < (10 - $largo); $i++) {
											$numero = "0".$numero;
										}
									}
									/* // INICIALIZAMOS LOS VALORES DE LA TRANSACCION // */
									$newTransaccion->numero_transaccion = $numero;
									$newTransaccion->numero_tarjeta     = $request->numero_tarjeta;
									$newTransaccion->codigo_terminal    = $request->codigo;
									$newTransaccion->tipo               = Transaccion::$TIPO_CONSUMO;
									$newTransaccion->fecha              = Carbon::now();
									$newTransaccion->sucursal_id        = $terminal->getSucursal->id;
									$newTransaccion->save();
									//creamos una nueva instancia del hestados transacciones
									$newEstadoTransacion                 = new HEstadoTransaccion();
									$newEstadoTransacion->transaccion_id = $newTransaccion->id;//asociamos la transaccion
									$newEstadoTransacion->estado         = HEstadoTransaccion::ESTADO_ACTIVO; // la definimos en estado activa
									$newEstadoTransacion->fecha          = Carbon::now(); //obtenemos la fecha actual
									$newEstadoTransacion->save(); //guadamos el estado de la transaccion
									foreach ($servicios as $servicio) {//recorremos los servicios enviados en la peticion
										if ($valorConsumir > 0) {//si el valor a consumir es mayor a 0
											if ($servicio == Tarjetas::CODIGO_SERVICIO_REGALO) {//si el servicio es regalo.
												//obtenemos los detalles productos correspondiente a regalo para la tarjeta
												$detalleProdutos = DetalleProdutos::where('numero_tarjeta', $request->numero_tarjeta)
																				  ->where('estado', DetalleProdutos::ESTADO_ACTIVO)
																				  ->where('factura', '<>', null)
																				  ->whereDate('fecha_vencimiento', '>', Carbon::now())
																				  ->orderBy('fecha_vencimiento', 'asc')
																				  ->get();
												//recorremos los detalles productos
												foreach ($detalleProdutos as $detalleProduto) {
													//buscamos las transacciones transacciones que se han hecho para este detalleproducto, y su valor total
													$transaciones =
														Transaccion::join('h_estado_transacciones', 'transacciones.id', 'h_estado_transacciones.transaccion_id')
																   ->join('detalle_transacciones', 'transacciones.id', 'detalle_transacciones.transaccion_id')
																   ->where('transacciones.numero_tarjeta', $request->numero_tarjeta)
																   ->where('detalle_transacciones.detalle_producto_id', $detalleProduto->id)
																   ->where('h_estado_transacciones.estado', 'A')
																   ->groupBy('transacciones.numero_tarjeta')
																   ->select(\DB::raw('SUM(valor) as total'))
																   ->get();
													//verificamos que exista sado en el detalle para consumir
													if ($transaciones->count()) {//Sitenemos el tranasacciones para la tarjeta, restamos lo gastado al monto inicial
														$porconsumir = $detalleProduto->monto_inicial - $transaciones[0]->total;
													}
													else {//si no tenemos tranasaciones lo que queda por consumir es el mismo valor de la tarjeta
														$porconsumir = $detalleProduto->monto_inicial;
													}
													if ($porconsumir > 0 && $valorConsumir > 0) {//de existir saldo, consumimos
														//verificamos que lo consumido deje en 0 el servicio
														$consumir = $porconsumir - $valorConsumir;
														if ($consumir < 0) { // si nos consumimos el valor total del saldo del producto
															//creamos un nuevo detalle de la transaccion
															$newDetalle                      = new DetalleTransaccion();
															$newDetalle->transaccion_id      = $newTransaccion->id;//asociamos la transaccion
															$newDetalle->detalle_producto_id = $detalleProduto->id; //asociamos el detalleproducto
															$newDetalle->valor               = $porconsumir;//agregamos el valor consumido a la tarjeta
															$newDetalle->descripcion         =
																DetalleTransaccion::$DESCRIPCION_CONSUMO;//definimos el tipo de transaccion
															$newDetalle->save();//guardamos los cambios
															$detalleProduto->estado =
																DetalleProdutos::ESTADO_CONSUMIDO;//cambios el estado del detalle producto a consumido
															$detalleProduto->save();//guardamos los cambios del detaller producto

															$algo = $this->crearHtarjeta($tarjeta->id,
																Motivo::CODIGO_M_SERVICIO_CONSUMIDO,
																$servicio,
																$tarjeta->estado,
																DetalleProdutos::ESTADO_CONSUMIDO
															);///creamos un htarjeta para el producto

															$consumus      = $this->returnaConsumos($consumus, $servicio,
																$porconsumir);//retornamos el valor consumido por el servicio
															$valorConsumir = $valorConsumir - $porconsumir;//obtenemos el valor restante por consumir
														}
														else {// de tener mayor saldo en el servicio, del que se va a consumir
															//creamos un nuevo detalle transaccion
															$newDetalle                      = new DetalleTransaccion();
															$newDetalle->transaccion_id      = $newTransaccion->id; //asociamos la transaccion
															$newDetalle->detalle_producto_id = $detalleProduto->id; //asociamos el detalle producto
															$newDetalle->valor               = $valorConsumir; //colocamos el valor a consumir
															$newDetalle->descripcion         =
																DetalleTransaccion::$DESCRIPCION_CONSUMO; //definimos el tipo de transaccion
															$newDetalle->save();//guardamos los cambios
															$consumus      = $this->returnaConsumos($consumus, $servicio, $valorConsumir);
															$valorConsumir = 0;
														}
													}

												}
											}
											elseif ($servicio == Tarjetas::CODIGO_SERVICIO_BONO) {//si el servicio aconsumir es bono empresarial
												//buscamos todos los detaller productos de la tarjeta por concepto bobo empresarial
												$detalleProdutos = DetalleProdutos::where('numero_tarjeta', $request->numero_tarjeta)
																				  ->where('estado', DetalleProdutos::ESTADO_ACTIVO)
																				  ->where('CONTRATO_EMPRS_ID', '<>', null)
																				  ->whereDate('fecha_vencimiento', '>', Carbon::now())
																				  ->orderBy('fecha_vencimiento', 'asc')
																				  ->get();
												//recorremos todos los detalles productos
												foreach ($detalleProdutos as $detalleProduto) {
													//obtenemos las transacciones por detaller porducto
													$transaciones =
														Transaccion::join('h_estado_transacciones', 'transacciones.id', 'h_estado_transacciones.transaccion_id')
																   ->join('detalle_transacciones', 'transacciones.id', 'detalle_transacciones.transaccion_id')
																   ->where('transacciones.numero_tarjeta', $request->numero_tarjeta)
																   ->where('detalle_transacciones.detalle_producto_id', $detalleProduto->id)
																   ->where('h_estado_transacciones.estado', 'A')
																   ->groupBy('transacciones.numero_tarjeta')
																   ->select(\DB::raw('SUM(valor) as total'))
																   ->get();
													//si el detalle producto tiene transacciones
													if ($transaciones->count()) {
														//descontamos las tranascciones consumidas al monto inicial del detalle producto
														$porconsumir = $detalleProduto->monto_inicial - $transaciones[0]->total;
													}
													else {
														//de no tener transaccion el valor por consumir es igual al monto inicial del detalle producto
														$porconsumir = $detalleProduto->monto_inicial;
													}

													if ($porconsumir > 0
														&& $valorConsumir > 0) { //si el detalle tiene saldo para consumir, y valor a consumir es mayor a 0
														$consumir = $porconsumir - $valorConsumir;
														if ($consumir < 0) { //si el valor del saldo es inferiror al valor por consumir
															$newDetalle                      = new DetalleTransaccion();//creamos un nuevo detalle transaccion
															$newDetalle->transaccion_id      = $newTransaccion->id;//asociamos la transaccion al detalle
															$newDetalle->detalle_producto_id = $detalleProduto->id;//asociamos el detalleproducto
															$newDetalle->valor               = $porconsumir;//agregamos el valor consumido del detalle producto
															$newDetalle->descripcion         =
																DetalleTransaccion::$DESCRIPCION_CONSUMO; //definimos el tipo de transaccion
															$newDetalle->save(); //guardamos el detalle producto
															$detalleProduto->estado =
																DetalleProdutos::ESTADO_CONSUMIDO; //cambiamos el estado al detalle producto
															$detalleProduto->save();//guardamos los cambios del detalleproducto
															$this->crearHtarjeta($tarjeta->id,
																Motivo::CODIGO_M_SERVICIO_CONSUMIDO,
																$servicio,
																$tarjeta->estado,
																DetalleProdutos::ESTADO_CONSUMIDO
															);//creamos un historial a la tarjeta por detalleproducto consumido
															$consumus = $this->returnaConsumos($consumus, $servicio, $porconsumir);
														}
														else {//si el el saldo de la tarjeta es superior al valor por consumir
															//creamos un nuevo detalle transaccion
															$newDetalle                      = new DetalleTransaccion();
															$newDetalle->transaccion_id      =
																$newTransaccion->id; //asociamos la transacion al detalle de la transacion
															$newDetalle->detalle_producto_id =
																$detalleProduto->id; //asociamos el detalleproducto al detalle de la transacion
															$newDetalle->valor               = $valorConsumir; //agregamos el valor consumido
															$newDetalle->descripcion         =
																DetalleTransaccion::$DESCRIPCION_CONSUMO; //definimos el tipo de transacion

															$newDetalle->save(); //guardamos los cambios

															$consumus      = $this->returnaConsumos($consumus, $servicio, $valorConsumir);
															$valorConsumir = 0; //definimos el valor a consumir en 0
														}
													}
												}
											}
										}
									}
									if ($valorConsumir == 0) {//si el valor a consumir es 0
										\DB::commit();//guardamos los cambios en base de datos
										$dt = Carbon::now();//obtenemos la fecha actual
										/*///// CONTRUIMOS LA RESPUESTA AL CLIENTE *///////
										$result['estado']             = true;
										$result['numero_transaccion'] = $newTransaccion->numero_transaccion;
										$result['fecha']              = $dt->toDateString();
										$result['hora']               = $dt->toTimeString();
										$result['codigoTerminal']     = $terminal->codigo;
										$result['numeroTarjeta']      = $tarjeta->numero_tarjeta;
										$result['tipoTransaccion']    = Transaccion::$TIPO_CONSUMO;
										$result['valor']              = $request->valor;
										$result['detalleServicio']    = $request->servicios;
										if ($tarjeta->persona_id != null) {
											$persona           = Personas::find($tarjeta->persona_id);
											$result['cedula']  = $persona->identificacion;
											$result['nombres'] = $persona->nombres." ".$persona->apellidos;
										}
										else {
											$result['nombres'] = "";
											$result['cedula']  = "";
										}
										$result['consumos'] = $consumus;

										//retornamos la respuesta al cliente
										return response()->json([
											KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
											KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TRANSACCION_EXITOSA,
											KeysResponse::KEY_DATA    => $result,
										], CodesResponse::CODE_OK);
									}
									else { // de ser superior a 0 el valor por consumir
										\DB::rollback();//descartamos los cambios en base de datos
										//retornamos una respuesta de error al cliente con saldo insuficiente
										return response()->json([
											KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
											KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TRANSACCION_INSUFICIENTE,
											KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TRANSACCION_INSUFICIENTE],
										], CodesResponse::CODE_BAD_REQUEST);
									}
								}
								else {//si la contraseña es incorrecta retornamos error al cliente
									return response()->json([
										KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
										KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
										KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_INCORECTO],
									], CodesResponse::CODE_BAD_REQUEST);
								}
							}
							else {//si la tarjeta esta inacitva retornamos error al cliente
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
						else {//si la tarjeta no es valida retornamos error al cliente
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_NO_VALIDA,
								KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_NO_VALIDA],
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
					else {//si la terminal esta inactiva retornamos error al cliente
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_INACTIVA,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_INACTIVA],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {// si la terminal no existe retornamos error al cliente
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_NO_EXISTE],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				\DB::rollback(); // de ocurrir algun error no contrlado descartamos cambios en base de datos
				//retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION.' '.$exception->getMessage(),
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}


		private function returnaConsumos($array, $servicio, $valor) {
			$existe = false;
			if (count($array)) {
				foreach ($array as $kery => $item) {

					if ($item['servicio'] == $servicio) {
						//dd(intval($item['valor']));
						$array[$kery]['valor_consumido'] = intval($item['valor_consumido']) + $valor;
						$existe                          = true;
					}

				}
			}

			if (!$existe) {
				$nservicio = '';
				switch ($servicio) {
					case 'R':
						$nservicio = 'Regalo';
						break;
					case 'B':
						$nservicio = 'Bono empresarial';
						break;
					case 'C':
						$nservicio = 'Cupo rotativo';
						break;
				}
				$data = [
					'servicio'        => $servicio,
					'nombre_servicio' => $nservicio,
					'valor_consumido' => $valor,
				];
				array_push($array, $data);
			}

			return $array;

		}

		/**
		 * metodo que permite traer el saldo de la tarjeta para los servicios que tenga disponible
		 *
		 * @param Request $request
		 * -codigo
		 * -numero_tarjeta
		 * -password
		 *
		 * @return array
		 */
		public function saldoTarjeta(Request $request) {
			try {
				$terminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($terminal != null) {
					if ($terminal->estado == Terminales::$TERMINAL_ESTADO_ACTIVA) {
						$tarjeta = Tarjetas::query()->where('numero_tarjeta', $request->numero_tarjeta)->first();
						if ($tarjeta != null) {
							if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {
								if ($request->password == Encript::decryption($tarjeta->password)) {
									$numero_tarjeta = $request->numero_tarjeta;
									$resultado      = [];
									$listado        = [];
									array_push($listado, $numero_tarjeta);

									$detalles = DetalleProdutos::wherein('numero_tarjeta', $listado)
															   ->where('estado', DetalleProdutos::ESTADO_ACTIVO)
															   ->get();
									foreach ($detalles as $detalle) {
										$gasto    = 0;
										$listadod = [];
										array_push($listadod, $detalle->id);

										$dtransacciones = DetalleTransaccion::wherein('detalle_producto_id', $listadod)->get();//$detalle->id
										foreach ($dtransacciones as $dtransaccione) {
											$htransaccion = DB::table('h_estado_transacciones')
															  ->where('transaccion_id', $dtransaccione->transaccion_id)
															  ->orderBy('id', 'desc')
															  ->first();
											if ($htransaccion->estado == HEstadoTransaccion::ESTADO_ACTIVO) {
												$gasto += $dtransaccione->valor;
											}
										}
										$sobrante = $detalle->monto_inicial - $gasto;
										$sobrante = number_format($sobrante, 2, ',', '.');
										if ($detalle->contrato_emprs_id == null) {
											$codigo_servicio = Tarjetas::CODIGO_SERVICIO_REGALO;
											$servicio        = 'Regalo';
										}
										else {
											$codigo_servicio = Tarjetas::CODIGO_SERVICIO_BONO;
											$servicio        = 'Bono empresarial';
										}
										$resultado[] = [
											'codigo_servicio' => $codigo_servicio,
											'servicio'        => $servicio,
											'saldo'           => $sobrante,
										];
									}

									return response()->json([
										KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
										KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TRANSACCION_EXITOSA,
										KeysResponse::KEY_DATA    => ['Saldos' => $resultado],
									], CodesResponse::CODE_OK);
								}
								else {
									return response()->json([
										KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
										KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
										KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_INCORECTO],
									], CodesResponse::CODE_BAD_REQUEST);
								}
							}
							else {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
						else {
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_NO_VALIDA,
								KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_NO_VALIDA],
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_INACTIVA,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_INACTIVA],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_NO_EXISTE],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}

		}

		/**
		 * metodo para anular una transaccion
		 *
		 * @param Request $request
		 * -codigo
		 * -numero_tarjeta
		 * -numero_transaccion
		 * -identificacion
		 * -password
		 *
		 * @return array
		 */
		public function anulacion(Request $request) {
			try {
				$terminal = Terminales::query()->where('codigo', $request->codigo)->first();
				if ($terminal != null) {
					if ($terminal->estado == Terminales::$ESTADO_TERMINAL_ACTIVA) {
						$tarjeta = Tarjetas::where('numero_tarjeta', $request->numero_tarjeta)->first();
						if ($tarjeta != null) {
							if ($tarjeta->estado == Tarjetas::ESTADO_TARJETA_ACTIVA) {
								if ($request->password == Encript::decryption($tarjeta->password)) {
									if ($tarjeta->persona_id != null) {
										$persona = Personas::query()->find($tarjeta->persona_id);
										if ($persona->documento == $request->documento) {
											$nombre = $persona->nombres." ".$persona->apellidos;
											$result = $this->anulaTransaccion($request, $terminal->codigo, $tarjeta->numero_tarjeta, $persona->identificacion,
												$nombre);
										}
										else {
											return response()->json([
												KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
												KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_DOCUMENTIO_INCORRECTO,
												KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_DOCUMENTIO_INCORRECTO],
											], CodesResponse::CODE_BAD_REQUEST);
										}
									}
									else {
										$result = $this->anulaTransaccion($request, $terminal->codigo, $tarjeta->numero_tarjeta, "", "");
									}
								}
								else {
									return response()->json([
										KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
										KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_PASSWORD_INCORECTO,
										KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_PASSWORD_INCORECTO],
									], CodesResponse::CODE_BAD_REQUEST);
								}
							}
							else {
								return response()->json([
									KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
									KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
									KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
								], CodesResponse::CODE_BAD_REQUEST);
							}
						}
						else {
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TARJETA_INACTIVA,
								KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TARJETA_INACTIVA],
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_INACTIVA,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_INACTIVA],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_TERMINAL_NO_EXISTE,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_TERMINAL_NO_EXISTE],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}

			return $result;
		}

		/**
		 * metodo que permite consultar el valor de una transaccion
		 *
		 * @param Request $request
		 * -codigo_terminal
		 * -numero_transaccion
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function saldoTransaccion(Request $request) {
			try {
				if ($request->has('numero_transaccion') && $request->has('codigo_terminal')) {
					$transaccion = Transaccion::query()
											  ->withCount([
												  'detalleTransaccion as total_transaccion' => function ($query) {
													  $query->select([\DB::raw('SUM(valor)')]);
												  },
											  ])
											  ->whereHas('htransanccionone', function ($query) {
												  $query->where('estado', HEstadoTransaccion::ESTADO_ACTIVO);
											  })
											  ->where('numero_transaccion', $request->numero_transaccion)
											  ->where('codigo_terminal', $request->codigo_terminal)
											  ->first();
					if ($transaccion != null) {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
							KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_QUERY_SUCCESS,
							KeysResponse::KEY_DATA    => ['transaccion' => $transaccion],
						], CodesResponse::CODE_OK);
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => 'No existe transaccion para esta terminal',
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => 'Debe enviar numero de terminal y codigo terminal',
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION,
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * metodo complementrario a la anulacion
		 *
		 * @param $request
		 *
		 * @return array
		 */
		private function anulaTransaccion($request, $codigo, $numero_tarjeta, $cedula, $nombres) {
			try {
				$transaccion = Transaccion::with('htransanccionone')->where('numero_transaccion', $request->numero_transaccion)
										  ->whereHas('htransanccionone', function ($query) {
											  $query->where('estado', HEstadoTransaccion::ESTADO_ACTIVO);
										  })->first();
				if ($transaccion != null) {
					if($transaccion->htransanccionone->estado == HEstadoTransaccion::ESTADO_INACTIVO){
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => "La transaccion se encuentra inactiva",
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
						], CodesResponse::CODE_BAD_REQUEST);
					}
					if ($transaccion->codigo_terminal == $request->codigo) {
						$fecha_hoy = Carbon::now()->format('Y-m-d');
						if (Carbon::createFromFormat('Y-m-d H:i:s', $transaccion->fecha)->toDateString() == $fecha_hoy) {
							$hestado                 = new HEstadoTransaccion();
							$hestado->transaccion_id = $transaccion->id;
							$hestado->estado         = HEstadoTransaccion::ESTADO_INACTIVO;
							$hestado->fecha          = Carbon::now();
							$hestado->save();
							$dt                           = Carbon::now();

							$numero = $transaccion->numero_transaccion;
							$largo = strlen($numero);
							for ($i = 0; $i < (10 - $largo); $i++) {
								$numero = "0".$numero;
							}

							$result['estado']             = true;
							$result['numero_transaccion'] = $numero;
							$result['fecha']              = $dt->toDateString();
							$result['hora']               = $dt->toTimeString();
							$result['codigoTerminal']     = $codigo;
							$result['numeroTarjeta']      = $numero_tarjeta;
							$result['tipoTransaccion']    = Transaccion::$TIPO_CONSUMO;
							$consumido                    = DetalleTransaccion::query()->where('transaccion_id', $transaccion->id)
																			  ->select([DB::raw('SUM(valor) as total')])
																			  ->groupBy('transaccion_id')->first();
							$result['valor']              = $consumido->total;// ----> esta dalo lo queme se nesecita el valor de la transaccion
							//$result['detalleServicio'] = "R"; /// ---->> aun no se necesita para nasda pero nose si mas adelante si
							$result['nombres'] = $nombres;
							$result['cedula']  = $cedula;
							$result['mensaje'] = 'Transaccion anulada';

							$detallesTransacciones = DetalleTransaccion::with('detalleProducto')->where('transaccion_id', $transaccion->id)->get();
							$array = [];
							foreach ($detallesTransacciones as $detallesTransaccione) {
								$data = [];
								DetalleProdutos::query()->where('id', $detallesTransaccione->detalleproducto_id)
														   ->update(['estado'=>DetalleProdutos::ESTADO_ACTIVO]);

								if($detallesTransaccione->detalleProducto->factura != ''){
									$data = [
										'servicio'        => 'R',
										'nombre_servicio' => 'Regalo',
										'valor_consumido' => $detallesTransaccione->valor,
									];
								}
								elseif($detallesTransaccione->detalleProducto->contrato_emprs_id != null){
									$data = [
										'servicio'        => 'B',
										'nombre_servicio' => 'Bono empresarial',
										'valor_consumido' => $detallesTransaccione->valor,
									];
								}

								array_push($array, $data);
							}

							$result['consumos'] = $array;

							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
								KeysResponse::KEY_MESSAGE => 'Transaccion anulada',
								KeysResponse::KEY_DATA    => $result,
							], CodesResponse::CODE_OK);
						}
						else {
							return response()->json([
								KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
								KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_FECHA_INVALIDA,
								KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_FECHA_INVALIDA],
							], CodesResponse::CODE_BAD_REQUEST);
						}
					}
					else {
						return response()->json([
							KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
							KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_NUMERO_TRANSACCION_NO_CORRESPONDE,
							KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_NUMERO_TRANSACCION_NO_CORRESPONDE],
						], CodesResponse::CODE_BAD_REQUEST);
					}
				}
				else {
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_NUMERO_TRANSACCION_INVALIDO,
						KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_NUMERO_TRANSACCION_INVALIDO],
					], CodesResponse::CODE_BAD_REQUEST);
				}
			} catch (\Exception $exception) {
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => ApiWS::$TEXT_ERROR_EJECUCION.' '.$exception->getMessage(),
					KeysResponse::KEY_DATA    => ['codigo' => ApiWS::$CODIGO_ERROR_EJECUCION],
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}


	}
