<?php

	namespace creditocofrem\Http\Controllers;

	use creditocofrem\Establecimientos;
	use creditocofrem\Globals\CodesResponse;
	use creditocofrem\Globals\KeysResponse;
	use creditocofrem\Globals\MessageResponse;
	use creditocofrem\Municipios;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Http\Request;
	use SoapFault;
	use Yajra\Datatables\Datatables;
	use creditocofrem\Empresas;
	use creditocofrem\Departamentos;
	use creditocofrem\TipoDocumento;
	use SoapClient;


	class EmpresasController extends Controller {

		/**
		 * trae la vista donde se listan todas las empresas de la red cofrem
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index() {
			return view('empresas.listaempresas');
		}

		/**
		 * metodo que carga la grid, en donde se listan todas las empresas de la red cofrem
		 *
		 * @return retorna el arreglo de tal manera que el datatable lo entienda
		 */
		public function gridEmpresas() {
			//consulto todas las empresas traigo la relacion que tiene con tipodocumento, municipio y departamentos
			$empresas = Empresas::with('getMunicipio.getDepartamento', 'getTipoDocumento')->get();

			//retorno las empresas en el formato esperado por el datatable en el cliente
			return Datatables::of($empresas)
							 ->addColumn('action', function ($empresas) {
								 $acciones = '<div class="btn-group">';
								 $acciones = $acciones.'<a class="btn btn-xs btn-info" data-modal="" href="'.route('empresa.editar', ['id' => $empresas->id])
									 .'"   ><i class="ti-pencil-alt"></i> Editar</a>';
								 if ($empresas->tipo == "A") // si el tipo de empresa es afiliada le muestro el boton de ir a convenios
								 {
									 $acciones = $acciones.'<a href="'.route('empresas.convenios', ['id' => $empresas->id])
										 .'" class="btn btn-xs btn-custom" ><i class="ti-layers-alt">&nbsp;Convenios</i> </a>'; //data-modal class="btn btn-xs btn-custom"
								 }
								 $acciones = $acciones.'</div>';

								 return $acciones;
							 })
							 ->make(true);
		}

		/**
		 * retorna la vista del modal que permite crear nuevas empresas
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewCrearEmpresa() {
			//obtengo la lista de departamentos
			$departamentos = Departamentos::pluck('descripcion', 'codigo');
			//obtiengo los tipos de documento
			$tipoDocumentos =  TipoDocumento::all();

			/*
				retorno la vista junto con la data correspondiente a los departamentos y tipos documentos
				para ser mostrados en los select del formulario
			*/

			return view('empresas.modalcrearempresas', compact('departamentos', 'tipoDocumentos'));
		}

		/**
		 * metodo que permite agregar un nuevo establecimiento al sistema
		 *
		 * @param Request $request trae la informacion del formulario, para agregar el establecimiento
		 *
		 * @return mixed returna una respuesta positiva o negativa dependiendo de la transaccion
		 */
		public function crearEmpresa(Request $request)//que datos son importantes que queden al crear la empresa
		{
			try {
				//validamos que el nit y el email sean unicos en la tabla empresas
				$validator = \Validator::make($request->all(), [
					'nit'   => 'required|unique:empresas|max:11',
					'email' => 'required|unique:empresas',
				]);

				if ($validator->fails()) { // de presentarse algun error en la validacion, le retornamos error a la vista
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}

				$empresa = new Empresas(); // Instanciamos un modelo vacio de empresa
				$empresa->fill($request->all()); // inicializamos los atributos del modelo con lo que trae el request

				if ($empresa->tipo == "Afiliado") // si el tipo de empresa que trajo el servicio soap es Afiliado
				{
					$empresa->tipo = Empresas::AFILIADO;
				} //cambiamos el valor por el esperado por la base de datos
				else {
					$empresa->tipo = Empresas::TERCERO;
				} //Si no es afiliado, es tercero, cambiamos el valor por el esperado por la base datos

				$empresa->razon_social = strtoupper($empresa->razon_social); // pasamos a mayuscula el valor de razon social
				$empresa->save();// guardamos en la base de datos la empresa

				//retornamos la respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_SUCCESS,
					KeysResponse::KEY_DATA    => $empresa,
				], CodesResponse::CODE_CREATED);

			} catch (\Exception $exception) {
				//si ocurre algun error la retornamos al cliente como error
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * retorna el modal con el formulario para editar la empresa
		 *
		 * @param \Illuminate\Http\Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function viewEditarEmpresa(Request $request) {
			try {
				$empresa       = Empresas::query()->findOrFail($request->id); //consulta la empresa a editar
				$depar         =
					Municipios::query()->find($empresa->municipio_codigo)->getDepartamento; // obtengo el valor del departamento atravez de sus relaciones
				$departamentos = Departamentos::pluck('descripcion', 'codigo'); //obtengo la lista de departamentos para mostrar en el select del formulario
				$tipoDocu      = TipoDocumento::pluck('equivalente', 'tip_codi'); // obtengo los tipos de documentos para mostrar en el select del formulario
				// retornamos la vista, junto con la data necesaria para mostrar
				return view('empresas.editarempresa', compact('empresa', 'departamentos', 'depar', 'tipoDocu'));
			} catch (ModelNotFoundException $exception) {
				return response()->json($exception->getMessage(), CodesResponse::CODE_NOT_FOUND);
			}

		}

		/**
		 * metodo que permite editar un establecimiento comercial
		 *
		 * @param Request $request campos del establecimiento a editar
		 *
		 * @return mixed
		 */
		public function editarEmpresa(Request $request) {
			try {
				//obtenemos la empresa que se desea editar
				$empresa = Empresas::query()->findOrFail($request->getQueryString());

				//validamos que el nit o el email sea unicos en la tabla empresa, siempre y cuando se haya sea valores diferentes al actual
				$validator = \Validator::make($request->all(), [
					'nit'   => 'required|max:10|unique:empresas,nit,'.$empresa->id,
					'email' => 'required|unique:empresas,email,'.$empresa->id,
				]);


				if ($validator->fails()) { // de presentarse algun error en la validacion, le retornamos error a la vista
					return response()->json([
						KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
						KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CREATE_ERROR,
						KeysResponse::KEY_DATA    => $validator->errors()->all(),
					], CodesResponse::CODE_FORM_INVALIDATE);
				}

				$empresa->update($request->all()); //actualizamos los datos del modelo exmpresa por los que traemos del request

				if ($request->tipo_name == "Afiliado") //dependiendo del tipo que trae el request, escogemos el tipo de empresa
				{
					$empresa->tipo = Empresas::AFILIADO;
				}
				else {
					$empresa->tipo = Empresas::TERCERO;
				}

				$empresa->razon_social = strtoupper($empresa->razon_social);// pasamos a mayusculas el valor de la razon social
				$empresa->save();// guardamos la empresa en base de datos

				//retornamos la respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_UPDATE_SUCCESS,
					KeysResponse::KEY_DATA    => $empresa,
				], CodesResponse::CODE_CREATED);
			} catch (\Exception $exception) {
				//si ocurre algun error la retornamos al cliente como error
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $exception->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

		/**
		 * METODO para consultar datos de aportante con WS de SevenAs
		 *
		 * @return mixed returna una objeto de tipo empresa, con los datos consultados (si existe) o vacia (si no la encontro)
		 */
		public function consultarAportante(Request $request) {
			$empresa = new Empresas(); // creamos un modelo vacio de empresa;
			$url     = "http://192.168.0.188/WebServices4/WConsultasCajas.asmx?wsdl"; //definimos la url para el servicio SOAP
			try {
				$client = new SoapClient($url, ["trace" => 1, "exception" => 0]); //inializamos el cliente SOAP

				//ejecutamos el metodo CosultaAportante del servicio SOAP
				$result = $client->ConsultaAportante([
					"emp_codi" => 406,
					"tip_codi" => $request->tipo,
					"apo_coda" => $request->num,
				]);

				if (isset($result->ConsultaAportanteResult->Aportante->TOAportante)) { // si se optiene resultado llenamos el model empresa
					$empresa->razon_social        = $result->ConsultaAportanteResult->Aportante->TOAportante->Apo_razs;
					$empresa->representante_legal = $result->ConsultaAportanteResult->Aportante->TOAportante->Ter_noco;
					$municipio_name               = $result->ConsultaAportanteResult->Aportante->TOAportante->Mun_nomb;
					$depto_name                   = $result->ConsultaAportanteResult->Aportante->TOAportante->Dep_nomb;
					$empresa->email               = $result->ConsultaAportanteResult->Aportante->TOAportante->Dsu_mail;
					$empresa->telefono            = $result->ConsultaAportanteResult->Aportante->TOAportante->Dsu_tele;
					$empresa->celular             = $result->ConsultaAportanteResult->Aportante->TOAportante->Dsu_celu;
					$empresa->direccion           = $result->ConsultaAportanteResult->Aportante->TOAportante->Dsu_dire;
					$empresa->tipo                = Empresas::AFILIADO; // si el SOAP da respuesta, definimos la empresa como tipo Afiliado

					//buscar departamento por el nombre en la base de dato
					if($depto_name == "DISTRITO CAPITAL"){
						$depto_name = "BOGOTÁ, D.C.";
						$municipio_name = "BOGOTÁ, D.C.";
					}

					$depto = Departamentos::where("descripcion", $depto_name)->first();
					if ($depto != null) { // si encontramos el departamento buscamos el municipio
						$muni = Municipios::where("descripcion", $municipio_name)
										  ->where("departamento_codigo", $depto->codigo)
										  ->first();
						$empresa->municipio_codigo    = $muni->codigo;
						$empresa->departamento_codigo = $depto->codigo;
					}
				}
				else {// si no obtenemos respuesta del sericio SOAP
					$empresa->tipo = Empresas::TERCERO; // definimos que el tipo de empresa es tercero
				}

				//reornamos una respuesta positiva al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_QUERY_SUCCESS,
					KeysResponse::KEY_DATA    => $empresa,
				], CodesResponse::CODE_OK);

			} catch (SoapFault $e) { // si ocurre algun erro al consultar el servicio retornamos el error al cliente
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $e->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}catch (\Exception $e){
				return response()->json([
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_ERROR,
					KeysResponse::KEY_MESSAGE => $e->getMessage(),
					KeysResponse::KEY_DATA    => null,
				], CodesResponse::CODE_INTERNAL_SERVER);
			}
		}

	}

