<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TipoTarjeta extends Model implements AuditableContract
{

	const ESTADO_ACTIVO= "A";
	const ESTADO_INACTIVO= "I";

	use Auditable;

	protected $fillable = [
		'nombre', 'estado'
	];


	public function servicios()
	{
		return $this->belongsToMany(Servicios::class, 'servicio_tipo_tarjeta');
	}

	public function tarjetas(){
		return $this->hasMany(Tarjetas::class,'tipo_tarjetas_id','id');
	}


}
