<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ConveniosEsta extends Model implements AuditableContract
{
    use Auditable;

	const ACTIVA = 'A';
	const INACTIVA = 'I';
	const PENDIENTE = 'P';

    protected $fillable = [
        'numero_convenio', 'fecha_inicio', 'fecha_fin','prorrogable'
    ];

    public function getEstablecimiento()
    {
        return $this->belongsTo('creditocofrem\Establecimientos','establecimiento_id','id');
    }
}
