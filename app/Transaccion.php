<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class Transaccion extends Model
{
    public static $TIPO_ADMINISTRATIVO = "A";
    public static $TIPO_CONSUMO = "C";

    protected $table = 'transacciones';


	protected $fillable = [
		'numero_transaccion',
		'numero_tarjeta',
		'codigo_terminal',
		'sucursal_id',
		'tipo',
		'fecha'
	];

    public function getSucursal(){
       return $this->belongsTo('creditocofrem\Sucursales','sucursal_id','id');
    }

    public function valorTransacion(){
        return $this->hasMany('creditocofrem\DetalleTransaccion','transaccion_id','id')->select([\DB::raw('SUM(valor) as total')]);
    }

    public function htransanccionone(){
    	return $this->hasOne(HEstadoTransaccion::class)->orderBy('id','desc');
	}

	public function htransanccion(){
		return $this->hasMany(HEstadoTransaccion::class);
	}

	public function detalleTransaccion(){
    	return $this->hasMany(DetalleTransaccion::class);
	}

}
