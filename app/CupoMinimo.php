<?php

namespace creditocofrem;

use Illuminate\Database\Eloquent\Model;

class CupoMinimo extends Model
{
    //
	const ACTIVO = 'A';
	const INACTIVO = 'I';

	protected $fillable = [
		'cupominimo',
		'estado',
		'servicio_codigo'
	];
}
