<div id="modaleditarcontrato">

    {{Form::model($contrato, ['route'=>['contrato.editarp', $contrato->id], 'files'=>'true', 'class'=>'form-horizontal', 'id'=>'editarcontrato'])}}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Editar contrato</h4>
    </div>
    <div class="modal-body">


        <div class="form-group">
            <label class="col-md-4 control-label">Número de contrato</label>
            <div class="col-md-8">
                {{Form::text('n_contrato', null ,['class'=>'form-control', "required", "tabindex"=>"1", 'id'=>'n_contrato', "disabled"])}}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Valor contrato</label>
            <div class="col-md-8">
                {{Form::text('valor_contrato', null ,['class'=>'form-control money', "required", "tabindex"=>"2",'id'=>'valor_contrato'])}}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Valor del impuesto</label>
            <div class="col-md-8">
                {{Form::text('valor_impuesto', null ,['class'=>'form-control money', "required", "tabindex"=>"3",'id'=>'impuesto'])}}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Fecha</label>
            <div class="col-md-8">
                {{Form::text("fecha", null,['class'=>'form-control', "required", "tabindex"=>"4", 'id'=>'fecha' ])}}
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label">Empresa</label>
            <div class="col-md-8">
                {{Form::text('nit',$empresa->nit,  ['class'=>'form-control', "tabindex"=>"5", "maxlength"=>"10", "disabled",  'id'=>'nit'])}}
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label">Número de tarjetas</label>
            <div class="col-md-8">
                {{Form::text('n_tarjetas', null ,['class'=>'form-control', "required", "data-parsley-type"=>"number", "maxlength"=>"3", "tabindex"=>"6", 'id'=>'n_tarjetas', "disabled"])}}
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label">Forma de pago</label>
            <div class="col-md-8">
                {{Form::select('forma_pago', ['E' => 'Efectivo', 'C' => 'Consumo'], 'E',['class'=>'form-control', "tabindex"=>"7", 'id'=>'forma_pago'])}}
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-4 control-label">Documentos</label>
            <div class="col-md-8">
                {{Form::file('pdf', ['class'=>'form-control',  "tabindex"=>"8", 'id'=>'pdf'])}}
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">&nbsp;</label>
            @if($contrato->pdf!=null)

                <div class="col-md-8">
                    <a href="{{url($contrato->pdf)}}" class="btn btn-primary form-control" target="_blank">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> ver {{$contrato->pdf}}
                    </a>
                </div>
            @endif
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Porcentaje Administración</label>
            <div class="col-md-8">
                {{Form::select("adminis_tarjeta_id",$administracion,null,['class'=>'form-control', "tabindex"=>"10", 'id'=>'administracion'])}}
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-custom waves-effect waves-light">Guardar</button>
    </div>
    {{Form::close()}}
</div>


<script>

    $(function () {

        $("#pdf").filestyle({
            buttonText: "Buscar archivo",
        });

        var valini = $('input[type=radio]').val();
        if (valini == 1) {
            $('#dias_consumo').attr('disabled', true);
        } else {
            $('#dias_consumo').removeAttr('disabled');
        }
        $('input[type=radio]').change(function () {
            if ($(this).val() == 1) {
                $('#dias_consumo').attr('disabled', true);
            } else {
                $('#dias_consumo').removeAttr('disabled');
            }
        });

        $('#fecha').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: 'es',
            todayHighlight: true,
            //startView: moment($('#fecha').val()).format('DD/MM/YYYY')
        });

        setTimeout(function () {
            $('#fecha').val(moment($('#fecha').val()).format('DD/MM/YYYY'));
        }, 300);

        $("#editarcontrato").parsley();
        $("#editarcontrato").submit(function (e) {
            e.preventDefault();
            var form = $(this);

            var formData = new FormData(form[0]);
            // formData.append( 'pdf', $( '#pdf' )[0].files[0] );

            $.ajax({
                type: "POST",
                context: document.body,
                url: form.attr('action'),
                processData: false,
                contentType: false,
                data: formData,
                beforeSend: function () {
                    cargando();
                },
                success: function (result) {
                    if (result.status) {
                        swal(
                            {
                                title: 'Bien!!',
                                text: result.message,
                                type: 'success',
                                confirmButtonColor: '#4fa7f3'
                            }
                        );
                    }
                    table.ajax.reload();
                    modalBs.modal('hide');
                },
                error: function (xhr, status) {
                    var mensajefinal;
                    var message = "Error de ejecución: " + xhr.status + " " + xhr.statusText;
                    mensajefinal = message + '\n';
                    mensajefinal += xhr.responseJSON.message+'\n';
                    if (xhr.status === 422) {
                        var errores="";
                        for (i = 0; i < xhr.responseJSON.data.length; i++) {
                            errores += xhr.responseJSON.data[i] + '\n';
                        }
                        mensajefinal += errores;
                        swal({
                            type: 'error',
                            title: 'Error...',
                            text: mensajefinal,
                        })
                    } else {
                        swal(
                            'Error!!',
                            mensajefinal
                            ,
                            'error'
                        )
                    }
                },
                // código a ejecutar sin importar si la petición falló o no
                complete: function (xhr, status) {
                    fincarga();
                }
            });
        });


    });


</script>