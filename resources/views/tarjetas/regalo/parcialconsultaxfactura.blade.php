


<div class="card-box widget-inline">
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="widget-inline-box">
                <button type="button" id="activar" class="btn btn-custom waves-effect waves-light" onclick="activar('{{$factura}}')">Activar todas</button>
            </div>
        </div>
    </div>
</div>
<div class="table-responsive m-b-20">
    <br>

    <table id="datatable" class="table table-striped table-bordered" width="100%">
        <thead>
        <tr>
            <th>Número tarjeta</th>
            <th>Número de factura</th>
            <th>Monto</th>
            <th>Estado</th>
        </tr>
        </thead>
        <tbody>
          @foreach($tarjetas as $tarjeta)
            <tr>
                <td>{{$tarjeta->numero_tarjeta}}</td>
                <td>{{$tarjeta->detallep->factura}}</td>
                <td>{{$tarjeta->detallep->monto_inicial}}</td>
                <td>{{$tarjeta->detallep->estado}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<script>
    $('#datatable').DataTable({
        "language": {
            "url": "{!!route('datatable_es')!!}"
        },
    });
    function activar(factura) {
        swal({
                title: '¿Estas seguro?',
                text: "¡Desea activar todas las tarjetas regalo asociadas a esta factura!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            },
            function () {
                $.ajax({
                    url: "{{route('regalo.activarxcfactura')}}",
                    data: {'factura': factura},
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function () {
                        cargando();
                    },
                    success: function (result) {
                        responseSuccess('Bien!!', result.message);
                        $('#tablaoculta').load('{{route('reagalo.consultaxfactura')}}',{factura:factura});
                    },
                    error: function (xhr, status) {
                        responseError(xhr);
                    },
                    complete: function (xhr, status) {
                        fincarga();
                    }
                });
            });
    }
</script>