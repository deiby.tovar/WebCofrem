<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicioTipoTarjeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_tipo_tarjeta', function (Blueprint $table) {
            $table->unsignedInteger("tipo_tarjeta_id");
            $table->unsignedInteger("servicios_id");
            $table->timestamps();

            $table->foreign('tipo_tarjeta_id')->references('id')->on('tipo_tarjetas')->onDelete('cascade');
            $table->foreign('servicios_id')->references('id')->on('servicios')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_tipo_tarjeta');
    }
}
