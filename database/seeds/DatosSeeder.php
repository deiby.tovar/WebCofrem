<?php

	use creditocofrem\Establecimientos;
	use creditocofrem\Sucursales;
	use creditocofrem\Terminales;
	use Illuminate\Database\Seeder;
	use Facades\creditocofrem\Encript;

class DatosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $establecimineto = new Establecimientos([
		   'nit' => 9002564853,
		   'razon_social' => "EXITOS",
		   'email' => "exitos@entuvida.com",
		   'telefono' => "31385855",
		   'celular' => "31385855",
		   'estado' => "I",
	   ]);

		$establecimineto->save();


		$sucursal = new Sucursales([
			'nombre' => "EL EXITO DE LA VIDAD",
			'latitud' => "4.1460891",
			'longitud' => "-73.63151519999997",
			'municipio_codigo' => "50001",
			'email' => "exitos@entuvida.com",
			'telefono' => "5555555555",
			'contacto' => "pepito"
		]);

		$sucursal->direccion = "CL 31 #27-12 ";
		$sucursal->estado = "I";
		$sucursal->establecimiento_id = $establecimineto->id;
		$sucursal->password = Encript::encryption("1234");
		$sucursal->save();


		$terminal = new Terminales([
			'sucursal_id' => $sucursal->id,
			'codigo' => "000000000000001",
			'celular' => "3131313131",
			'numero_activo' => "1",
			'password' => Encript::encryption("1234"),
			'estado' => "I",
		]);

		$terminal->save();


    }
}
